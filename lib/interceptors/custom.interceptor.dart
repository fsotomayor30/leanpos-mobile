import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' as getx;
import 'package:logger/logger.dart';

class CustomInterceptors extends InterceptorsWrapper {
  var logger = Logger();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    print(
        "\x1B[32m REQUEST[${options?.method}] => PATH: ${options?.path} \x1B[0m");

    // print("[CONTENT TYPE] " + options.contentType);
    print("[HEADER] " + options.headers.toString());

    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print(
        "\x1B[36m RESPONSE[${response?.statusCode}] => PATH: ${response?.requestOptions?.path} \x1B[0m");
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    print(
        "\x1B[31m ERROR[${err?.response?.statusCode}] => PATH: ${err?.requestOptions?.path} \x1B[0m");
    logger.e('[ERROR INTERCEPTOR] ${err.toString()}');

    logger.e('[ERROR INTERCEPTOR] ${err.response}');
    logger.e('[ERROR CODE] ${err.response.statusCode}');

    if (err?.response?.statusCode != 404) {
      EasyLoading.dismiss();

      getx.Get.snackbar(
        'Error',
        err.response.toString(),
        snackPosition: getx.SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        colorText: Colors.white,
      );
    }

    if (err?.response?.statusCode == 404) {
      EasyLoading.dismiss();

      getx.Get.snackbar(
        'Error',
        'Usuario incorrecto',
        snackPosition: getx.SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        colorText: Colors.white,
      );
    }

    return super.onError(err, handler);
  }
}
