import 'package:dio/dio.dart';
import 'package:leanpos/entities/dto/indicadores_obtener.dto.dart';
import 'package:leanpos/entities/indicadores.entity.dart';
import 'package:leanpos/entities/sucursal.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class ReporteriaService {
  ReporteriaService._internal();

  static ReporteriaService _instance = ReporteriaService._internal();

  static ReporteriaService get instance => _instance;

  Future<List<Sucursal>> obtenerSucursales() async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    List<Sucursal> sucursales = [];

    response =
        await dio.get(environment['ENDPOINT'] + "/Reporteria/GetSucursales");

    response.data.forEach((data) {
      final dataTemp = Sucursal.fromJson(data);
      sucursales.add(dataTemp);
    });

    return sucursales;
  }

  Future<Indicadores> obtenerIndicadores(
      IndicadoresObtenerDto indicadoresObtenerDto) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());

    response = await dio.post(
        environment['ENDPOINT'] + "/Reporteria/PostIndicadores",
        data: indicadoresObtenerDto.toJson());

    return Indicadores.fromJson(response.data);
  }
}
