import 'package:dio/dio.dart';
import 'package:leanpos/entities/modificador.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class ModificadorService {
  ModificadorService._internal();

  static ModificadorService _instance = ModificadorService._internal();

  static ModificadorService get instance => _instance;

  Future<List<Modificador>> obtenerModificadores() async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    List<Modificador> modificadores = [];

    response = await dio.get(environment['ENDPOINT'] + "/Tbmodifs");

    response.data.forEach((data) {
      final dataTemp = Modificador.fromJson(data);
      modificadores.add(dataTemp);
    });

    return modificadores;
  }
}
