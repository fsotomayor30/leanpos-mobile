import 'package:dio/dio.dart';
import 'package:leanpos/entities/dto/promocion_por_producto.dto.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class Promociones {
  Promociones._internal();

  static Promociones _instance = Promociones._internal();

  static Promociones get instance => _instance;

  Future<PromocionPorProductoDto> obtenerPromocionesPorProducto(
      int productoId) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());

    response = await dio.get(environment['ENDPOINT'] +
        "/Promociones/GetPromocionesProducto?productoID=$productoId");
    return PromocionPorProductoDto.fromJson(response.data);
  }
}
