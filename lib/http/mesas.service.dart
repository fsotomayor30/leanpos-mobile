import 'package:dio/dio.dart';
import 'package:leanpos/entities/ambiente.entity.dart';
import 'package:leanpos/entities/mesa.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class MesasService {
  MesasService._internal();

  static MesasService _instance = MesasService._internal();

  static MesasService get instance => _instance;

  Future<List<Mesa>> obtenerMesaPorSucursal(int sucursal) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
    ));
    dio.interceptors.add(CustomInterceptors());
    List<Mesa> mesas = [];

    response = await dio.get(environment['ENDPOINT'] + "/Mesas/GetMesasDos");

    response.data.forEach((data) {
      final dataTemp = Mesa.fromJson(data);
      if (dataTemp.sucursalId == sucursal) {
        mesas.add(dataTemp);
      }
    });

    return mesas;
  }

  Future<List<Ambiente>> obtenerAmbientes(int sucursal) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
    ));
    dio.interceptors.add(CustomInterceptors());
    List<Ambiente> ambientes = [];

    response = await dio.get(
        environment['ENDPOINT'] + "/Mesas/GetAmbientes?SucursalID=$sucursal");

    response.data.forEach((data) {
      final dataTemp = Ambiente.fromJson(data);
      ambientes.add(dataTemp);
    });

    return ambientes;
  }
}
