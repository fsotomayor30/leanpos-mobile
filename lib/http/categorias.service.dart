import 'package:dio/dio.dart';
import 'package:leanpos/entities/categoria.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class CategoriasService {
  CategoriasService._internal();

  static CategoriasService _instance = CategoriasService._internal();

  static CategoriasService get instance => _instance;

  Future<List<Categoria>> obtenerCategorias(int sucursal) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
        // followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    List<Categoria> categorias = [];

    response = await dio.get(environment['ENDPOINT'] +
        "/tbgrupoes/TraerGrupos?SucursalID=$sucursal");

    response.data.forEach((data) {
      final dataTemp = Categoria.fromJson(data);
      categorias.add(dataTemp);
    });

    return categorias;
  }
}
