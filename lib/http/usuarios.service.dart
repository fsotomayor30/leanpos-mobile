import 'dart:math';

import 'package:dio/dio.dart';
import 'package:leanpos/entities/user.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class UsuariosService {
  UsuariosService._internal();

  static UsuariosService _instance = UsuariosService._internal();

  static UsuariosService get instance => _instance;

  Future<Usuario> obtenerUsuario(String pin) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    print(pin);
    print(environment['ENDPOINT']);
    response = await dio
        .get(environment['ENDPOINT'] + "/Usuarios/" + pin + '?pin=true');

    return Usuario.fromJson(response.data);
  }
}
