import 'package:dio/dio.dart';
import 'package:leanpos/entities/dto/orden_actualizar.dto.dart';
import 'package:leanpos/entities/dto/orden_crear.dto.dart';
import 'package:leanpos/entities/orden.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class OrdenesService {
  OrdenesService._internal();

  static OrdenesService _instance = OrdenesService._internal();

  static OrdenesService get instance => _instance;

  Future<List<Orden>> obtenerOrdenPorMesa(int mesa) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    List<Orden> ordenes = [];

    response = await dio
        .get(environment['ENDPOINT'] + "/Orden/GetOrdenByMesa?idMesa=$mesa");

    response.data.forEach((data) {
      final dataTemp = Orden.fromJson(data);
      ordenes.add(dataTemp);
    });

    return ordenes;
  }

  Future<bool> sendToPrintCloud(int id) async {
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());

    try {
      await dio.get(
          environment['ENDPOINT'] + "/Orden/SendToPrintCloud?OrdenID=${id}");
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<Orden> obtenerOrdenPorId(int id) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());

    response = await dio.get(environment['ENDPOINT'] + "/Orden/${id}");

    return Orden.fromJson(response.data);
  }

  Future<bool> obtenerEstadoPagadoOrdenPorIdYSucursal(
      int id, int idSucursal) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    response = await dio.get(environment['ENDPOINT'] +
        "/Orden/GetEstadoOrden?sucursalID=$idSucursal&ordenID=$id");

    return response.data;
  }

  Future<void> actualizarOrden(
      int id, OrdenActualizarDto ordenActualizarDto) async {
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    await dio.put(environment['ENDPOINT'] + "/Orden/$id",
        data: ordenActualizarDto.toJson());
  }

  Future<Orden> crearOrden(OrdenCrearDto ordenCrearDto) async {
    Response response;

    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    response = await dio.post(environment['ENDPOINT'] + "/Orden",
        data: ordenCrearDto.toJson());

    return Orden.fromJson(response.data);
  }
}
