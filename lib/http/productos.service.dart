import 'package:dio/dio.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/interceptors/custom.interceptor.dart';
import 'package:leanpos/.env.dart';

class ProductosService {
  ProductosService._internal();

  static ProductosService _instance = ProductosService._internal();

  static ProductosService get instance => _instance;

  Future<List<Producto>> obtenerProductos(int sucursal, int categoria) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());
    List<Producto> productos = [];

    response = await dio.get(environment['ENDPOINT'] +
        "/tbproductoes/GetProductoPorGrupoIdParaLlevar?id=$categoria&SucursalID=$sucursal");

// response = await dio.get(environment['ENDPOINT'] +
//         "/tbproductoes/GetProductoPorGrupoId?id=$categoria&SucursalID=$sucursal");

    response.data.forEach((data) {
      final dataTemp = Producto.fromJson(data);
      productos.add(dataTemp);
    });

    return productos;
  }

  Future<Producto> obtenerProductoConModificadores(int id) async {
    Response response;
    Dio dio = new Dio(BaseOptions(
//      followRedirects: false,
        ));
    dio.interceptors.add(CustomInterceptors());

    response = await dio.get(
        environment['ENDPOINT'] + "/tbproductoes/GettbproductoConModif?id=$id");

    return Producto.fromJson(response.data);
  }
}
