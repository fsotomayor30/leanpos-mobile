import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/login/controllers/login.controller.dart';

class NumberButtonWidget extends StatefulWidget {
  String number;
  NumberButtonWidget({@required this.number});

  @override
  _NumberButtonWidgetState createState() => _NumberButtonWidgetState();
}

class _NumberButtonWidgetState extends State<NumberButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: LoginController(),
        builder: (_) => GestureDetector(
              onTap: () {
                _.setPassword(widget.number);
              },
              child: Container(
                height: 81,
                width: 81,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(81)),
                    color: Color(0xFFE6EDE9)),
                child: Center(
                  child: Text(widget.number, style: TextStyle(fontSize: 30)),
                ),
              ),
            ));
  }
}
