import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/login/controllers/login.controller.dart';

class InputNumberWidget extends StatefulWidget {
  final Function onChanged;
  InputNumberWidget({@required this.onChanged});

  @override
  _InputNumberWidgetState createState() => _InputNumberWidgetState();
}

class _InputNumberWidgetState extends State<InputNumberWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: LoginController(),
      builder: (_) => TextFormField(
        textAlign: TextAlign.center,
          controller: _.textEditingController,
          obscureText: true,
          enabled: false,
          keyboardType: TextInputType.phone,
          inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
          decoration: InputDecoration(
              contentPadding: EdgeInsets.zero,
              border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.transparent),
            ),
            // hintText: widget.hint,
            filled: false,
          ),
          style: new TextStyle(color: Color(0xFF707070), fontSize: 50, height: 0),
          onChanged: (String value){
            this.widget.onChanged(value);
          }
      ),
    );

  }
}
