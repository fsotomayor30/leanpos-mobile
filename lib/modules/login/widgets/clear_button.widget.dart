import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';

import '../controllers/login.controller.dart';

class ClearButtonWidget extends StatefulWidget {
  @override
  _ClearButtonWidgetState createState() => _ClearButtonWidgetState();
}

class _ClearButtonWidgetState extends State<ClearButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: LoginController(),
        builder: (LoginController _) => GestureDetector(
              onTap: () {
                _.textEditingController.clear();
              },
              child: Container(
                height: 81,
                width: 81,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(81)),
                    color: Color(0xFFCC555D)),
                child: Center(
                  child: FaIcon(
                    FontAwesomeIcons.backspace,
                    color: Colors.white,
                  ),
                ),
              ),
            ));
  }
}
