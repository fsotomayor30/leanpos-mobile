import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class EnterButtonWidget extends StatefulWidget {
  @override
  _EnterButtonWidgetState createState() => _EnterButtonWidgetState();
}

class _EnterButtonWidgetState extends State<EnterButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 81,
      width: 81,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(81)),
          color: Color(0xFF4AA144)),
      child: Center(
        child: FaIcon(
          FontAwesomeIcons.check,
          color: Colors.white,
        ),
      ),
    );
  }
}
