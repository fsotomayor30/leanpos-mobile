import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/categoria.entity.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/entities/user.entity.dart';
import 'package:leanpos/http/categorias.service.dart';
import 'package:leanpos/http/productos.service.dart';
import 'package:leanpos/http/usuarios.service.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

import '../../home/pages/home.page.dart';

class LoginController extends GetxController {
  TextEditingController _textEditingController = TextEditingController();
  TextEditingController get textEditingController => _textEditingController;

  void setPassword(String number) {
    if (_textEditingController.text.length >= 4) {
      _textEditingController.text = '';
    }
    _textEditingController.text = _textEditingController.text + number;
    update();
  }

  Future<void> login() async {
    GlobalController _globalController = Get.find();

    EasyLoading.show(status: 'Iniciando sesión...', dismissOnTap: false);

    if (_textEditingController.text.length < 1) {
      EasyLoading.dismiss();
      Get.snackbar(
        'Error',
        'Formato de PIN Incorrecto',
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        colorText: Colors.white,
      );
      return;
    }

    Usuario usuario = await UsuariosService.instance
        .obtenerUsuario(_textEditingController.text);
    _globalController.setUsuario(usuario);
    EasyLoading.dismiss();

    EasyLoading.show(status: 'Cargando categorias...', dismissOnTap: false);

    List<Categoria> categories = await CategoriasService.instance
        .obtenerCategorias(_globalController.usuario.sucursalId);
    _globalController.setCategories(categories);
    EasyLoading.dismiss();

    _globalController.setCategorySelected(_globalController.categories[0]);

    EasyLoading.show(status: 'Cargando productos...', dismissOnTap: false);
    List<Producto> product = [];
    _globalController.categories.forEach((element) async {
      product.addAll(await ProductosService.instance
          .obtenerProductos(_globalController.usuario.sucursalId, element.id));
    });

    _globalController.setProduct(product);
    EasyLoading.dismiss();

    Get.offAll(HomePage());
  }
}
