import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/login/widgets/clear_button.widget.dart';
import 'package:leanpos/modules/login/widgets/enter_button.widget.dart';
import 'package:leanpos/modules/login/widgets/input_number.widget.dart';
import 'package:leanpos/modules/login/widgets/number_button.widget.dart';

import '../controllers/login.controller.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: LoginController(),
        builder: (LoginController _) => Scaffold(
              body: Column(
                children: [
                  Expanded(child: Container()),
                  Image.asset('assets/leanpos.png'),
                  SizedBox(height: 50),
                  InputNumberWidget(onChanged: (String password) {}),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          NumberButtonWidget(number: '1'),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '2'),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '3'),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          NumberButtonWidget(number: '4'),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '5'),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '6'),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          NumberButtonWidget(number: '7'),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '8'),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '9'),
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          ClearButtonWidget(),
                          SizedBox(width: 10),
                          NumberButtonWidget(number: '0'),
                          SizedBox(width: 10),
                          GestureDetector(
                              onTap: () async {
                                await _.login();
                              },
                              child: EnterButtonWidget()),
                        ],
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                  Expanded(child: Container())
                ],
              ),
            ));
  }
}
