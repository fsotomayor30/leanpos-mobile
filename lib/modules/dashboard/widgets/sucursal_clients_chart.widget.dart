import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';

class SucursalClientsChartWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SucursalClientsChartWidgetState();
}

class SucursalClientsChartWidgetState
    extends State<SucursalClientsChartWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        builder: (DashboardController dashboardController) => Card(
              child: AspectRatio(
                aspectRatio: .8,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10.0, right: 10, bottom: 10, top: 10),
                  child: Container(
                    child: Stack(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    FaIcon(
                                      FontAwesomeIcons.users,
                                      size: 15,
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      'Clientes',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 15,
                            ),
                            dashboardController
                                    .indicators.ventasPorSucursal.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 16.0, left: 6.0),
                                      child: BarChart(
                                        data(dashboardController),
                                        swapAnimationDuration:
                                            const Duration(milliseconds: 250),
                                      ),
                                    ),
                                  ),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ));
  }

  BarChartData data(DashboardController dashboardController) {
    return BarChartData(
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff72719b),
            fontWeight: FontWeight.bold,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (double value) {
            return dashboardController.optionsSucursal[value.floor()];
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff75729e),
            fontWeight: FontWeight.bold,
            fontSize: 8,
          ),
          getTitles: (value) {
            return value.floor().toString();
          },
          margin: 8,
          reservedSize: 30,
        ),
      ),
      barGroups: dashboardController.generateDataSucursalClientsChart(),
      gridData: FlGridData(
        show: true,
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: Color(0xff4e4965),
            width: 2,
          ),
          left: BorderSide(
            color: Color(0xff4e4965),
            width: 2,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
    );
  }
}
