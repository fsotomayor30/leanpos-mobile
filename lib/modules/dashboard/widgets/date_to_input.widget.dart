import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';

class DateToInputWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: DashboardController(),
        builder: (DashboardController dashboardController) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                // mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      Text("Fecha Hasta:",
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold)),
                      SizedBox(width: 5),
                      Text(
                          dashboardController.dateTo
                              .toLocal()
                              .toString()
                              .split(' ')[0],
                          style: TextStyle(color: Color(0xFF707070))),
                    ],
                  ),
                  GestureDetector(
                      onTap: () => _selectDate(dashboardController),
                      child: FaIcon(FontAwesomeIcons.calendarAlt,
                          color: Color(0xFF59978c)))
                ],
              ),
            ));
  }

  Future<void> _selectDate(DashboardController dashboardController) async {
    final DateTime picked = await showDatePicker(
        context: Get.context,
        initialDate: dashboardController.dateTo,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != dashboardController.dateTo)
      dashboardController.setDateTo(picked);
  }
}
