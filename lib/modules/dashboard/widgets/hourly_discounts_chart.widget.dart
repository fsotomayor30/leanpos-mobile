import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';
import 'dart:math';

class HourlyDiscountsChartWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HourlyDiscountsChartWidgetState();
}

class HourlyDiscountsChartWidgetState
    extends State<HourlyDiscountsChartWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        builder: (DashboardController dashboardController) => Card(
              child: AspectRatio(
                aspectRatio: .9,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10.0, right: 10, bottom: 10, top: 10),
                  child: Container(
                    child: Stack(
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Align(
                                alignment: Alignment.centerLeft,
                                child: Row(
                                  children: [
                                    FaIcon(
                                      FontAwesomeIcons.percentage,
                                      size: 15,
                                    ),
                                    SizedBox(width: 10),
                                    Text(
                                      'Descuentos',
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 15,
                            ),
                            dashboardController.indicators.ventasPorHora.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                          right: 16.0, left: 6.0),
                                      child: LineChart(
                                        data(dashboardController),
                                        swapAnimationDuration:
                                            const Duration(milliseconds: 250),
                                      ),
                                    ),
                                  ),
                            const SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ));
  }

  LineChartData data(DashboardController dashboardController) {
    return LineChartData(
      gridData: FlGridData(
        show: true,
      ),
      titlesData: FlTitlesData(
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff72719b),
            fontWeight: FontWeight.bold,
            fontSize: 10,
          ),
          margin: 10,
          getTitles: (value) {
            return dashboardController.optionsHourly[value.floor()];
          },
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff75729e),
            fontWeight: FontWeight.bold,
            fontSize: 8,
          ),
          getTitles: (value) {
            return value.ceil().toString();
          },
          margin: 8,
          reservedSize: 30,
        ),
      ),
      borderData: FlBorderData(
        show: true,
        border: const Border(
          bottom: BorderSide(
            color: Color(0xff4e4965),
            width: 2,
          ),
          left: BorderSide(
            color: Color(0xff4e4965),
            width: 2,
          ),
          right: BorderSide(
            color: Colors.transparent,
          ),
          top: BorderSide(
            color: Colors.transparent,
          ),
        ),
      ),
      minX: 0,
      maxX: (dashboardController.optionsHourly.length - 1).toDouble(),
      maxY: 0,
      minY:
          dashboardController.dataHourlyDiscounts.reduce(min).toDouble() - 2000,
      lineBarsData: linesBarData(dashboardController),
    );
  }

  List<LineChartBarData> linesBarData(DashboardController dashboardController) {
    final lineChartBarData1 = LineChartBarData(
      spots: dashboardController.generateDataHourlyDiscountsChart(),
      isCurved: false,
      colors: [
        Color(0xFF59978c),
      ],
      barWidth: 2,
      isStrokeCapRound: true,
      dotData: FlDotData(
        show: true,
      ),
      belowBarData: BarAreaData(
        show: true,
        colors: [Color(0xFF59978c).withOpacity(.5)],
      ),
    );

    return [
      lineChartBarData1,
    ];
  }
}
