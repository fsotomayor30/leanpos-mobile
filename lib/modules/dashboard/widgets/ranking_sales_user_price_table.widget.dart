import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:leanpos/shared/utlils/number.formatter.dart';

class RankingSalesUserPriceTableWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        builder: (DashboardController dashboardController) => Card(
            child: AspectRatio(
                aspectRatio: .8,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 10.0, right: 10, bottom: 10, top: 10),
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: [
                              FaIcon(
                                FontAwesomeIcons.moneyBillAlt,
                                size: 15,
                              ),
                              SizedBox(width: 10),
                              Text(
                                'Precio',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                            ],
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      Expanded(child: _getBodyWidget(dashboardController)),
                    ],
                  ),
                ))));
  }

  Widget _getBodyWidget(DashboardController dashboardController) {
    return HorizontalDataTable(
      leftHandSideColumnWidth: 150,
      rightHandSideColumnWidth: 600,
      isFixedHeader: true,
      headerWidgets: _getTitleWidget(),
      leftSideItemBuilder: _generateFirstColumnRow,
      rightSideItemBuilder: _generateRightHandSideColumnRow,
      itemCount: dashboardController.indicators.rankingUsuarios.length,
      rowSeparatorWidget: const Divider(
        color: Colors.black54,
        height: 1.0,
        thickness: 0.0,
      ),
      leftHandSideColBackgroundColor: Color(0xFFFFFFFF),
      rightHandSideColBackgroundColor: Color(0xFFFFFFFF),
      verticalScrollbarStyle: const ScrollbarStyle(
        thumbColor: Color(0xFF59978c),
        isAlwaysShown: true,
        thickness: 4.0,
        radius: Radius.circular(5.0),
      ),
      horizontalScrollbarStyle: const ScrollbarStyle(
        thumbColor: Color(0xFF59978c),
        isAlwaysShown: true,
        thickness: 4.0,
        radius: Radius.circular(5.0),
      ),
      enablePullToRefresh: false,
      refreshIndicatorHeight: 60,
    );
  }

  List<Widget> _getTitleWidget() {
    return [
      _getTitleItemWidget('Usuario', 100),
      _getTitleItemWidget('Precio', 100),
      _getTitleItemWidget('Cantidad', 100),
      _getTitleItemWidget('% Cantidad', 100),
      _getTitleItemWidget('% Precio', 100),
    ];
  }

  Widget _getTitleItemWidget(String label, double width) {
    return Container(
      child: Text(label, style: TextStyle(fontWeight: FontWeight.bold)),
      width: width,
      height: 56,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateFirstColumnRow(BuildContext context, int index) {
    DashboardController dashboardController = Get.find();
    return Container(
      child: Text(
          dashboardController.indicators.rankingUsuarios[index].nombreUsuario),
      width: 100,
      height: 52,
      padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
      alignment: Alignment.centerLeft,
    );
  }

  Widget _generateRightHandSideColumnRow(BuildContext context, int index) {
    DashboardController dashboardController = Get.find();

    return Row(
      children: <Widget>[
        Container(
          child: Row(
            children: <Widget>[
              Text(moneyFormatter(dashboardController
                      .indicators.rankingUsuarios[index].valorTotal
                      .ceil())
                  .toString())
            ],
          ),
          width: 100,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(dashboardController
              .indicators.rankingUsuarios[index].cantidad
              .toString()),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(dashboardController
              .indicators.rankingUsuarios[index].porcentajeCantidad
              .toString()),
          width: 100,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
        Container(
          child: Text(dashboardController
              .indicators.rankingUsuarios[index].porcentajePrecio
              .toString()),
          width: 200,
          height: 52,
          padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
          alignment: Alignment.centerLeft,
        ),
      ],
    );
  }
}
