import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';
import 'package:leanpos/shared/utlils/number.formatter.dart';

class ResumeChartWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        builder: (DashboardController dashboadController) => Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    AspectRatio(
                      aspectRatio: 1.3,
                      child: Column(
                        children: <Widget>[
                          const SizedBox(
                            height: 20,
                          ),
                          Align(
                              alignment: Alignment.centerLeft,
                              child: Row(
                                children: [
                                  FaIcon(FontAwesomeIcons.moneyBill),
                                  SizedBox(width: 10),
                                  Text(
                                    'Ventas',
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              )),
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: dashboadController.indicators.ventas
                                .map((e) => Indicator(
                                      color: dashboadController
                                          .colorResumeChart[e.tipoVenta],
                                      text: e.tipoVenta,
                                      isSquare: false,
                                    ))
                                .toList(),
                          ),
                          dashboadController.indicators.ventas
                                  .every((element) => element.venta == 0)
                              ? Container()
                              : const SizedBox(
                                  height: 18,
                                ),
                          dashboadController.indicators.ventas
                                  .every((element) => element.venta == 0)
                              ? Container()
                              : Expanded(
                                  child: AspectRatio(
                                    aspectRatio: 1,
                                    child: PieChart(
                                      PieChartData(
                                          startDegreeOffset: 180,
                                          borderData: FlBorderData(
                                            show: false,
                                          ),
                                          sectionsSpace: 1,
                                          centerSpaceRadius: 0,
                                          sections: showingSections(
                                              dashboadController)),
                                    ),
                                  ),
                                ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                              moneyFormatter(dashboadController
                                      .indicators.ventasTotales)
                                  .toString(),
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text('Total'),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                                moneyFormatter(dashboadController
                                    .indicators.ventasTotalesSinCs),
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            Text('TotalSinCS'),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ));
  }

  List<PieChartSectionData> showingSections(
      DashboardController dashboardController) {
    return dashboardController.indicators.ventas.map((e) {
      return PieChartSectionData(
        color: dashboardController.colorResumeChart[e.tipoVenta],
        value: e.venta.toDouble(),
        title: moneyFormatter(e.venta).toString(),
        radius: 80,
        titleStyle: TextStyle(
            fontSize: 18, fontWeight: FontWeight.bold, color: Colors.white),
        titlePositionPercentageOffset: 0.55,
      );
    }).toList();
  }
}

class Indicator extends StatelessWidget {
  final Color color;
  final String text;
  final bool isSquare;
  final double size;
  final Color textColor;

  const Indicator({
    @required this.color,
    @required this.text,
    @required this.isSquare,
    this.size = 16,
    this.textColor = const Color(0xff505050),
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          width: size,
          height: size,
          decoration: BoxDecoration(
            shape: isSquare ? BoxShape.rectangle : BoxShape.circle,
            color: color,
          ),
        ),
        const SizedBox(
          width: 4,
        ),
        Text(
          text,
          style: TextStyle(
              fontSize: 16, fontWeight: FontWeight.bold, color: textColor),
        )
      ],
    );
  }
}
