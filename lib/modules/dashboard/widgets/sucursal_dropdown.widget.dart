import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/sucursal.entity.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';

class SucursalDropdownWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: DashboardController(),
      builder: (DashboardController dashboardController) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: DropdownButton<Sucursal>(
          isExpanded: true,
          value: dashboardController.sucursalSelected,
          //elevation: 5,
          style: TextStyle(color: Colors.black),
          items: dashboardController.sucursals
              .map<DropdownMenuItem<Sucursal>>((Sucursal sucursal) {
            return DropdownMenuItem<Sucursal>(
              value: sucursal,
              child: Text(sucursal.descripcion),
            );
          }).toList(),
          hint: Text(
            "Escoge una sucursal",
            style: TextStyle(
                color: Colors.black, fontSize: 16, fontWeight: FontWeight.w600),
          ),
          onChanged: (Sucursal value) {
            dashboardController.setSucursalSelected(value);
          },
        ),
      ),
    );
  }
}
