import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';
import 'package:leanpos/shared/utlils/number.formatter.dart';

class ResumeCancellationsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        builder: (DashboardController dashboardController) => Card(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: [
                        FaIcon(FontAwesomeIcons.minus),
                        SizedBox(width: 10),
                        Text(
                          'Anulaciones',
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Column(
                          children: [
                            Text(
                                dashboardController.indicators.anulacionCantidad
                                    .toString(),
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            SizedBox(
                              height: 10,
                            ),
                            Text('CANTIDAD'),
                          ],
                        ),
                        Column(
                          children: [
                            Text(
                                moneyFormatter(dashboardController
                                        .indicators.anulacionAvg
                                        .ceil())
                                    .toString(),
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            SizedBox(
                              height: 10,
                            ),
                            Text('MONTO'),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ));
  }
}
