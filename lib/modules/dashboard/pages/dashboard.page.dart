import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:leanpos/modules/dashboard/controllers/dashbaord.controller.dart';
import 'package:leanpos/modules/dashboard/widgets/annual_cancellations_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/annual_clients_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/annual_discounts_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/annual_sales_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/annual_transactions_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/date_from_input.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/date_to_input.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/hourly_cancellations_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/hourly_clients_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/hourly_discounts_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/hourly_sales_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/sucursal_cancellations_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/sucursal_clients_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/sucursal_discounts_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/sucursal_dropdown.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/sucursal_sales_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/hourly_transactions_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_cancellation_user_price_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_cancellation_user_quantity_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_category_price_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_category_quantity_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_sales_user_price_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_sales_user_quantity_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_product_price_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/ranking_product_quantity_table.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/resume_cancellations.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/resume_chart.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/resume_clients.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/resume_discounts.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/resume_transactions.widget.dart';
import 'package:leanpos/modules/dashboard/widgets/sucursal_transactions_chart.widget.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: DashboardController(),
        builder: (DashboardController dashboardController) => Scaffold(
              appBar: AppBar(
                leading: GestureDetector(
                  onTap: () => Get.back(),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, left: 15),
                    child: FaIcon(
                      FontAwesomeIcons.arrowLeft,
                      color: Color(0xFF59978c),
                    ),
                  ),
                ),
                backgroundColor: Color(0xFFFFFFFF),
                centerTitle: true,
                title: Text('Dashboard',
                    style: TextStyle(color: Color(0xFF59978c))),
              ),
              body: Column(
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          SucursalDropdownWidget(),
                          DateFromInputWidget(),
                          SizedBox(
                            height: 10,
                          ),
                          DateToInputWidget(),
                          ElevatedButton(
                            child: Text('Buscar'),
                            onPressed: () async {
                              await dashboardController.loadIndicators();
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                  !dashboardController.loadingData
                      ? Container()
                      : Expanded(
                          child: ListView(
                            children: [
                              ExpansionTile(
                                title: Row(
                                  children: [
                                    FaIcon(FontAwesomeIcons.home),
                                    SizedBox(width: 10),
                                    Text(
                                      "Inicio",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                children: <Widget>[
                                  ResumeChartWidget(),
                                  ResumeTransactionsWidget(),
                                  ResumeClientsWidget(),
                                  ResumeCancellationsWidget(),
                                  ResumeDiscountsWidget()
                                ],
                              ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.stopwatch),
                                          SizedBox(width: 10),
                                          Text(
                                            "Gráfico por hora",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        HourlySalesChartWidget(),
                                        HourlyTransactionsChartWidget(),
                                        HourlyDiscountsChartWidget(),
                                        HourlyClientsChartWidget(),
                                        HourlyCancellationsChartWidget()
                                      ],
                                    ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.chartLine),
                                          SizedBox(width: 10),
                                          Text(
                                            "Ranking de productos",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        RankingProductQuantityTableWidget(),
                                        RankingProductPriceTableWidget()
                                      ],
                                    ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.chartLine),
                                          SizedBox(width: 10),
                                          Text(
                                            "Ranking de categoría",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        RankingCategoryQuantityTableWidget(),
                                        RankingCategoryPriceTableWidget()
                                      ],
                                    ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.chartLine),
                                          SizedBox(width: 10),
                                          Text(
                                            "Ranking de ventas usuarios",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        RankingSalesUserQuantityTableWidget(),
                                        RankingSalesUserPriceTableWidget()
                                      ],
                                    ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.chartLine),
                                          SizedBox(width: 10),
                                          Flexible(
                                            child: Text(
                                              "Ranking de anulaciones por usuario",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        RankingCancellationUserQuantityTableWidget(),
                                        RankingCancellationUserPriceTableWidget()
                                      ],
                                    ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.storeAlt),
                                          SizedBox(width: 10),
                                          Flexible(
                                            child: Text(
                                              "Gráfico por sucursal",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        SucursalSalesChartWidget(),
                                        SucursalTransactionsChartWidget(),
                                        SucursalDiscountsChartWidget(),
                                        SucursalClientsChartWidget(),
                                        SucursalCancellationsChartWidget(),
                                      ],
                                    ),
                              !dashboardController.loadingData
                                  ? Container()
                                  : ExpansionTile(
                                      title: Row(
                                        children: [
                                          FaIcon(FontAwesomeIcons.calendarAlt),
                                          SizedBox(width: 10),
                                          Flexible(
                                            child: Text(
                                              "Gráfico Anual",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 18.0,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ],
                                      ),
                                      children: <Widget>[
                                        AnnualSalesChartWidget(),
                                        AnnualTransactionsChartWidget(),
                                        AnnualDiscountsChartWidget(),
                                        AnnualClientsChartWidget(),
                                        AnnualCancellationsChartWidget()
                                      ],
                                    ),
                            ],
                          ),
                        ),
                ],
              ),
            ));
  }
}
