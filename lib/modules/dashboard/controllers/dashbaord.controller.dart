import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/dto/indicadores_obtener.dto.dart';
import 'package:leanpos/entities/indicadores.entity.dart';
import 'package:leanpos/entities/sucursal.entity.dart';
import 'package:leanpos/http/reporteria.service.dart';
import 'package:leanpos/shared/utlils/date.formatter.dart';

class DashboardController extends GetxController {
  Sucursal _sucursalSelected;
  Sucursal get sucursalSelected => _sucursalSelected;

  DateTime _dateFrom = DateTime.now();
  DateTime get dateFrom => _dateFrom;

  DateTime _dateTo = DateTime.now();
  DateTime get dateTo => _dateTo;

  List<Sucursal> _sucursals = [];
  List<Sucursal> get sucursals => _sucursals;

  Indicadores _indicators;
  Indicadores get indicators => _indicators;

  Map<String, Color> _colorResumeChart = {};
  Map<String, Color> get colorResumeChart => _colorResumeChart;

  bool _loadingData = false;
  bool get loadingData => _loadingData;

  List<String> _optionsHourly = [];
  List<String> get optionsHourly => _optionsHourly;

  List<String> _optionsAnnual = [];
  List<String> get optionsAnnual => _optionsAnnual;

  List<String> _optionsSucursal = [];
  List<String> get optionsSucursal => _optionsSucursal;

  List<int> _dataHourlySales = [];
  List<int> get dataHourlySales => _dataHourlySales;

  List<int> _dataSucursalSales = [];
  List<int> get dataSucursalSales => _dataSucursalSales;

  List<int> _dataSucursalTransactions = [];
  List<int> get dataSucursalTransactions => _dataSucursalTransactions;

  List<int> _dataSucursalDiscounts = [];
  List<int> get dataSucursalDiscounts => _dataSucursalDiscounts;

  List<int> _dataSucursalClients = [];
  List<int> get dataSucursalClients => _dataSucursalClients;

  List<int> _dataSucursalCancellations = [];
  List<int> get dataSucursalCancellations => _dataSucursalCancellations;

  List<int> _dataHourlyTransactions = [];
  List<int> get dataHourlyTransactions => _dataHourlyTransactions;

  List<int> _dataAnnualTransactions = [];
  List<int> get dataAnnualTransactions => _dataAnnualTransactions;

  List<int> _dataHourlyDiscounts = [];
  List<int> get dataHourlyDiscounts => _dataHourlyDiscounts;

  List<int> _dataAnnualDiscounts = [];
  List<int> get dataAnnualDiscounts => _dataAnnualDiscounts;

  List<int> _dataHourlyClients = [];
  List<int> get dataHourlyClients => _dataHourlyClients;

  List<int> _dataAnnualClients = [];
  List<int> get dataAnnualClients => _dataAnnualClients;

  List<int> _dataHourlyCancellations = [];
  List<int> get dataHourlyCancellations => _dataHourlyCancellations;

  List<int> _dataAnnualCancellations = [];
  List<int> get dataAnnualCancellations => _dataAnnualCancellations;

  List<int> _dataAnnualSales = [];
  List<int> get dataAnnualSales => _dataAnnualSales;

  void setDateFrom(DateTime dateFrom) {
    _dateFrom = dateFrom;
    update();
  }

  void setDateTo(DateTime dateTo) {
    _dateTo = dateTo;
    update();
  }

  void setSucursalSelected(Sucursal sucursalSelected) {
    _sucursalSelected = sucursalSelected;
    update();
  }

  void setOptionsHourly(List<String> optionsHourly) {
    _optionsHourly = optionsHourly;
    update();
  }

  void setOptionsAnnual(List<String> optionsAnnual) {
    _optionsAnnual = optionsAnnual;
    update();
  }

  void setOptionsSucursal(List<String> optionsSucursal) {
    _optionsSucursal = optionsSucursal;
    update();
  }

  void setDataHourlySales(List<int> dataHourlySales) {
    _dataHourlySales = dataHourlySales;
    update();
  }

  void setDataSucursalSales(List<int> dataSucursalSales) {
    _dataSucursalSales = dataSucursalSales;
    update();
  }

  void setDataSucursalDiscounts(List<int> dataSucursalDiscounts) {
    _dataSucursalDiscounts = dataSucursalDiscounts;
    update();
  }

  void setDataSucursalClients(List<int> dataSucursalClients) {
    _dataSucursalClients = dataSucursalClients;
    update();
  }

  void setDataSucursalCancellations(List<int> dataSucursalCancellations) {
    _dataSucursalCancellations = dataSucursalCancellations;
    update();
  }

  void setDataSucursalTransactions(List<int> dataSucursalTransactions) {
    _dataSucursalTransactions = dataSucursalTransactions;
    update();
  }

  void setDataHourlyTransactions(List<int> dataHourlyTransactions) {
    _dataHourlyTransactions = dataHourlyTransactions;
    update();
  }

  void setDataAnnualTransactions(List<int> dataAnnualTransactions) {
    _dataAnnualTransactions = dataAnnualTransactions;
    update();
  }

  void setDataHourlyDiscounts(List<int> dataHourlyDiscounts) {
    _dataHourlyDiscounts = dataHourlyDiscounts;
    update();
  }

  void setDataAnnualDiscounts(List<int> dataAnnualDiscounts) {
    _dataAnnualDiscounts = dataAnnualDiscounts;
    update();
  }

  void setDataHourlyClients(List<int> dataHourlyClients) {
    _dataHourlyClients = dataHourlyClients;
    update();
  }

  void setDataAnnualClients(List<int> dataAnnualClients) {
    _dataAnnualClients = dataAnnualClients;
    update();
  }

  void setDataHourlyCancellations(List<int> dataHourlyCancellations) {
    _dataHourlyCancellations = dataHourlyCancellations;
    update();
  }

  void setDataAnnualCancellations(List<int> dataAnnualCancellations) {
    _dataAnnualCancellations = dataAnnualCancellations;
    update();
  }

  void setDataAnnualSales(List<int> dataAnnualSales) {
    _dataAnnualSales = dataAnnualSales;
    update();
  }

  void setLoadingData(bool loadingData) {
    _loadingData = loadingData;
    update();
  }

  void setColorResumeChart(Map<String, Color> colorResumeChart) {
    _colorResumeChart = colorResumeChart;
    update();
  }

  void setIndicators(Indicadores indicators) {
    _indicators = indicators;
    update();
  }

  void setSucursals(List<Sucursal> sucursals) {
    _sucursals = sucursals;
    update();
  }

  @override
  void onInit() async {
    await loadSucursals();
    super.onInit();
  }

  Future<void> loadSucursals() async {
    EasyLoading.show(status: 'Cargando sucursales...', dismissOnTap: false);

    List<Sucursal> sucursals =
        await ReporteriaService.instance.obtenerSucursales();
    setSucursals(sucursals);
    EasyLoading.dismiss();
  }

  Future<void> loadIndicators() async {
    EasyLoading.show(status: 'Cargando indicadores...', dismissOnTap: false);

    IndicadoresObtenerDto indicadoresObtenerDto = IndicadoresObtenerDto(
        tipoComparativo: 'Semana',
        sucursal:
            sucursalSelected == null ? '0' : sucursalSelected.id.toString(),
        fechaIni: DateTime(dateFrom.year, dateFrom.month, dateFrom.day),
        fechaFin: DateTime(dateTo.year, dateTo.month, dateTo.day));

    Indicadores indicators = await ReporteriaService.instance
        .obtenerIndicadores(indicadoresObtenerDto);
    setIndicators(indicators);

    generateColorResumeChart();
    generateHourlyChart();

    generateSucursalChart();

    generateDataHourlySales();
    generateDataHourlyTransactions();
    generateDataHourlyDiscounts();
    generateDataHourlyClients();
    generateDataHourlyCancellations();

    generateDataSucursalSales();
    generateDataSucursalTransactions();
    generateDataSucursalDiscounts();
    generateDataSucursalClientes();
    generateDataSucursalCancellations();

    generateAnnualChart();
    generateDataAnnualSales();
    generateDataAnnualTransactions();
    generateDataAnnualDiscounts();
    generateDataAnnualClients();
    generateDataAnnualCancellations();

    EasyLoading.dismiss();
    setLoadingData(true);
  }

  void generateColorResumeChart() {
    final random = Random();

    Map<String, Color> mapColor = {};
    _indicators.ventas.forEach((element) {
      mapColor[element.tipoVenta] =
          Colors.primaries[random.nextInt(Colors.primaries.length)]
              [random.nextInt(9) * 100];
    });

    setColorResumeChart(mapColor);
  }

  generateHourlyChart() {
    List<String> optionsHourly = [];

    if (_indicators.ventasPorHora.length == 1) {
      optionsHourly.add('0');
    }

    for (var i = 0; i < _indicators.ventasPorHora.length; i++) {
      if (i % 2 == 0) {
        optionsHourly.add(_indicators.ventasPorHora[i].hora.toString() + 'Hr');
      } else {
        optionsHourly.add('');
      }
    }

    setOptionsHourly(optionsHourly);
  }

  generateAnnualChart() {
    List<String> optionsAnnual = [];

    if (_indicators.ventasPorAo.length == 1) {
      optionsAnnual.add('');
    }

    for (var i = 0; i < _indicators.ventasPorAo.length; i++) {
      if (i % 2 == 0) {
        optionsAnnual.add(
            getMonth(_indicators.ventasPorAo[i].mesInt).substring(0, 3) +
                "/" +
                _indicators.ventasPorAo[i].ao.toString().substring(2));
      } else {
        optionsAnnual.add('');
      }
    }

    setOptionsAnnual(optionsAnnual);
  }

  generateSucursalChart() {
    List<String> optionsSucursal = [];

    if (_indicators.ventasPorSucursal.length == 1) {
      optionsSucursal.add('');
    }

    _indicators.ventasPorSucursal.forEach((element) {
      optionsSucursal.add(element.sucursal);
    });

    setOptionsSucursal(optionsSucursal);
  }

  generateDataHourlySales() {
    List<int> dataHourlySales = [];
    if (_indicators.ventasPorHora.length == 1) {
      dataHourlySales.add(0);
    }

    _indicators.ventasPorHora.forEach((element) {
      dataHourlySales.add(element.ventaPorHora);
    });

    setDataHourlySales(dataHourlySales);
  }

  generateDataAnnualSales() {
    List<int> dataAnnualSales = [];
    if (_indicators.ventasPorAo.length == 1) {
      dataAnnualSales.add(0);
    }

    _indicators.ventasPorAo.forEach((element) {
      dataAnnualSales.add(element.ventaPorMes);
    });

    setDataAnnualSales(dataAnnualSales);
  }

  generateDataSucursalSales() {
    List<int> dataSucursalSales = [];
    if (_indicators.ventasPorSucursal.length == 1) {
      dataSucursalSales.add(0);
    }

    _indicators.ventasPorSucursal.forEach((element) {
      dataSucursalSales.add(element.ventaPorSuc);
    });

    setDataSucursalSales(dataSucursalSales);
  }

  generateDataSucursalTransactions() {
    List<int> dataSucursalTransactions = [];
    if (_indicators.ventasPorSucursal.length == 1) {
      dataSucursalTransactions.add(0);
    }

    _indicators.ventasPorSucursal.forEach((element) {
      dataSucursalTransactions.add(int.parse(element.cantidadTransacciones));
    });

    setDataSucursalTransactions(dataSucursalTransactions);
  }

  generateDataSucursalDiscounts() {
    List<int> dataSucursalDiscounts = [];
    if (_indicators.ventasPorSucursal.length == 1) {
      dataSucursalDiscounts.add(0);
    }

    _indicators.ventasPorSucursal.forEach((element) {
      dataSucursalDiscounts.add(element.descuentosSuc);
    });

    setDataSucursalDiscounts(dataSucursalDiscounts);
  }

  generateDataSucursalClientes() {
    List<int> dataSucursalClients = [];
    if (_indicators.ventasPorSucursal.length == 1) {
      dataSucursalClients.add(0);
    }

    _indicators.ventasPorSucursal.forEach((element) {
      dataSucursalClients.add(element.clientesSuc);
    });

    setDataSucursalClients(dataSucursalClients);
  }

  generateDataSucursalCancellations() {
    List<int> dataSucursalCancellations = [];
    if (_indicators.ventasPorSucursal.length == 1) {
      dataSucursalCancellations.add(0);
    }

    _indicators.ventasPorSucursal.forEach((element) {
      dataSucursalCancellations.add(element.anulacionesSuc);
    });

    setDataSucursalCancellations(dataSucursalCancellations);
  }

  List<BarChartGroupData> generateDataSucursalClientsChart() {
    List<BarChartGroupData> dataGraph = [];

    for (int indice = 0; indice < _dataSucursalClients.length; indice++) {
      dataGraph.add(BarChartGroupData(
        x: indice,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            y: _dataSucursalClients[indice].toDouble(),
            colors: [Color(0xFF59978c)],
            width: 22,
          ),
        ],
      ));
    }

    return dataGraph;
  }

  List<BarChartGroupData> generateDataSucursalCancellationsChart() {
    List<BarChartGroupData> dataGraph = [];

    for (int indice = 0; indice < _dataSucursalCancellations.length; indice++) {
      dataGraph.add(BarChartGroupData(
        x: indice,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            y: _dataSucursalCancellations[indice].toDouble(),
            colors: [Color(0xFF59978c)],
            width: 22,
          ),
        ],
      ));
    }

    return dataGraph;
  }

  List<BarChartGroupData> generateDataSucursalSalesChart() {
    List<BarChartGroupData> dataGraph = [];

    for (int indice = 0; indice < _dataSucursalSales.length; indice++) {
      dataGraph.add(BarChartGroupData(
        x: indice,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            y: _dataSucursalSales[indice].toDouble(),
            colors: [Color(0xFF59978c)],
            width: 22,
          ),
        ],
      ));
    }

    return dataGraph;
  }

  List<BarChartGroupData> generateDataSucursalDiscountsChart() {
    List<BarChartGroupData> dataGraph = [];

    for (int indice = 0; indice < _dataSucursalDiscounts.length; indice++) {
      dataGraph.add(BarChartGroupData(
        x: indice,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            y: _dataSucursalDiscounts[indice].toDouble(),
            colors: [Color(0xFF59978c)],
            width: 22,
          ),
        ],
      ));
    }

    return dataGraph;
  }

  List<BarChartGroupData> generateDataSucursalTransactionsChart() {
    List<BarChartGroupData> dataGraph = [];

    for (int indice = 0; indice < _dataSucursalTransactions.length; indice++) {
      dataGraph.add(BarChartGroupData(
        x: indice,
        barRods: [
          BarChartRodData(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            y: _dataSucursalTransactions[indice].toDouble(),
            colors: [Color(0xFF59978c)],
            width: 22,
          ),
        ],
      ));
    }

    return dataGraph;
  }

  List<FlSpot> generateDataHourlySalesChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataHourlySales.length; indice++) {
      dataGraph
          .add(FlSpot(indice.toDouble(), _dataHourlySales[indice].toDouble()));
    }

    return dataGraph;
  }

  List<FlSpot> generateDataAnnualSalesChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataAnnualSales.length; indice++) {
      dataGraph
          .add(FlSpot(indice.toDouble(), _dataAnnualSales[indice].toDouble()));
    }

    return dataGraph;
  }

  generateDataHourlyTransactions() {
    List<int> dataHourlyTransactions = [];
    if (_indicators.ventasPorHora.length == 1) {
      dataHourlyTransactions.add(0);
    }

    _indicators.ventasPorHora.forEach((element) {
      dataHourlyTransactions.add(int.parse(element.cantidadTransacciones));
    });

    setDataHourlyTransactions(dataHourlyTransactions);
  }

  generateDataAnnualTransactions() {
    List<int> dataAnnualTransactions = [];
    if (_indicators.ventasPorAo.length == 1) {
      dataAnnualTransactions.add(0);
    }

    _indicators.ventasPorAo.forEach((element) {
      dataAnnualTransactions.add(int.parse(element.cantidadTransacciones));
    });

    setDataAnnualTransactions(dataAnnualTransactions);
  }

  List<FlSpot> generateDataHourlyTransactionsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataHourlyTransactions.length; indice++) {
      dataGraph.add(FlSpot(
          indice.toDouble(), _dataHourlyTransactions[indice].toDouble()));
    }

    return dataGraph;
  }

  List<FlSpot> generateDataAnnualTransactionsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataAnnualTransactions.length; indice++) {
      dataGraph.add(FlSpot(
          indice.toDouble(), _dataAnnualTransactions[indice].toDouble()));
    }

    return dataGraph;
  }

  generateDataHourlyDiscounts() {
    List<int> dataHourlyDiscounts = [];
    if (_indicators.ventasPorHora.length == 1) {
      dataHourlyDiscounts.add(0);
    }

    _indicators.ventasPorHora.forEach((element) {
      dataHourlyDiscounts.add(element.descuentosPorHora);
    });

    setDataHourlyDiscounts(dataHourlyDiscounts);
  }

  generateDataAnnualDiscounts() {
    List<int> dataHourlyDiscounts = [];
    if (_indicators.ventasPorAo.length == 1) {
      dataHourlyDiscounts.add(0);
    }

    _indicators.ventasPorAo.forEach((element) {
      dataHourlyDiscounts.add(element.descuentosPorMes);
    });

    setDataAnnualDiscounts(dataHourlyDiscounts);
  }

  List<FlSpot> generateDataHourlyDiscountsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataHourlyDiscounts.length; indice++) {
      dataGraph.add(
          FlSpot(indice.toDouble(), _dataHourlyDiscounts[indice].toDouble()));
    }

    return dataGraph;
  }

  List<FlSpot> generateDataAnnualDiscountsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataAnnualDiscounts.length; indice++) {
      dataGraph.add(
          FlSpot(indice.toDouble(), _dataAnnualDiscounts[indice].toDouble()));
    }

    return dataGraph;
  }

  generateDataHourlyClients() {
    List<int> dataHourlyClients = [];
    if (_indicators.ventasPorHora.length == 1) {
      dataHourlyClients.add(0);
    }

    _indicators.ventasPorHora.forEach((element) {
      dataHourlyClients.add(element.clientesPorHora);
    });

    setDataHourlyClients(dataHourlyClients);
  }

  generateDataAnnualClients() {
    List<int> dataAnnualClients = [];
    if (_indicators.ventasPorAo.length == 1) {
      dataAnnualClients.add(0);
    }

    _indicators.ventasPorAo.forEach((element) {
      dataAnnualClients.add(element.clientesPorMes);
    });

    setDataAnnualClients(dataAnnualClients);
  }

  List<FlSpot> generateDataHourlyClientsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataHourlyClients.length; indice++) {
      dataGraph.add(
          FlSpot(indice.toDouble(), _dataHourlyClients[indice].toDouble()));
    }

    return dataGraph;
  }

  List<FlSpot> generateDataAnnualClientsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataAnnualClients.length; indice++) {
      dataGraph.add(
          FlSpot(indice.toDouble(), _dataAnnualClients[indice].toDouble()));
    }

    return dataGraph;
  }

  generateDataHourlyCancellations() {
    List<int> dataHourlyCancellations = [];
    if (_indicators.ventasPorHora.length == 1) {
      dataHourlyCancellations.add(0);
    }

    _indicators.ventasPorHora.forEach((element) {
      dataHourlyCancellations.add(element.anulacionesPorHora);
    });

    setDataHourlyCancellations(dataHourlyCancellations);
  }

  generateDataAnnualCancellations() {
    List<int> datAnnualCancellations = [];
    if (_indicators.ventasPorAo.length == 1) {
      datAnnualCancellations.add(0);
    }

    _indicators.ventasPorAo.forEach((element) {
      datAnnualCancellations.add(element.anulacionesMes);
    });

    setDataAnnualCancellations(datAnnualCancellations);
  }

  List<FlSpot> generateDataHourlyCancellationsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataHourlyCancellations.length; indice++) {
      dataGraph.add(FlSpot(
          indice.toDouble(), _dataHourlyCancellations[indice].toDouble()));
    }

    return dataGraph;
  }

  List<FlSpot> generateDataAnnualCancellationsChart() {
    List<FlSpot> dataGraph = [];

    for (int indice = 0; indice < _dataAnnualCancellations.length; indice++) {
      dataGraph.add(FlSpot(
          indice.toDouble(), _dataAnnualCancellations[indice].toDouble()));
    }

    return dataGraph;
  }
}
