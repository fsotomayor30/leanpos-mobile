import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/dto/orden_actualizar.dto.dart';
import 'package:leanpos/entities/dto/orden_crear.dto.dart';
import 'package:leanpos/entities/mesa.entity.dart';
import 'package:leanpos/entities/modificador.entity.dart';
import 'package:leanpos/entities/orden.entity.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/http/modificador.service.dart';
import 'package:leanpos/http/orden.service.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/login/pages/login.page.dart';
import 'package:leanpos/modules/table/controllers/table.controller.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class OrderController extends GetxController {
  TextEditingController _textModifierController = TextEditingController();
  TextEditingController get textModifierController => _textModifierController;

  bool _expanded = true;
  bool get expanded => _expanded;

  Orden _orden;
  Orden get orden => _orden;

  int _total = 0;
  int get total => _total;

  int _gavetaId = 0;
  int get gavetaId => _gavetaId;

  String _nombreCliente;
  String get nombreCliente => _nombreCliente;

  int _numeroClientes;
  int get numeroClientes => _numeroClientes;

  List<Producto> _carroProductosTemporal = [];
  List<Producto> get carroProductosTemporal => _carroProductosTemporal;

  List<Modificador> _modificadores = [];
  List<Modificador> get modificadores => _modificadores;

  void setCarroProductosTemporal(List<Producto> carroProductosTemporal) {
    _carroProductosTemporal = carroProductosTemporal;
    update();
  }

  void setModificadores(List<Modificador> modificadores) {
    _modificadores = modificadores;
    update();
  }

  void setExpanded(bool expanded) {
    _expanded = expanded;
    update();
  }

  // FUNCIONALIDAD STEP MODIFICADORES
  void eliminarModificador({String modificador, int indiceProducto}) {
    _carroProductosTemporal[indiceProducto]
        .modificadores
        .removeWhere((modificadorMap) => modificadorMap == modificador);
    update();
  }

  void agregarModificador({String modificador, int indiceProducto}) {
    _carroProductosTemporal[indiceProducto].modificadores.add(modificador);
    update();
  }

  void agregarModificadorTexto({int indiceProducto}) {
    if (_textModifierController.text.isEmpty) {
      Get.snackbar(
        'Faltan datos!',
        'Debes ingresar un mensaje',
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.red,
        colorText: Colors.white,
      );
      return;
    }
    _carroProductosTemporal[indiceProducto]
        .modificadores
        .add(_textModifierController.text);
    _modificadores.add(
        Modificador(desmod: _textModifierController.text, isProducto: false));
    _textModifierController.clear();
    update();
  }

  //

  void setTotal(int total) {
    _total = total;
    update();
  }

  void setNumeroClientes(int numeroClientes) {
    _numeroClientes = numeroClientes;
    update();
  }

  void setNombreCliente(String nombreCliente) {
    _nombreCliente = nombreCliente;
    update();
  }

  void setGavetaId(int gavetaId) {
    _gavetaId = gavetaId;
    update();
  }

  void setOrden(Orden orden) {
    _orden = orden;
    update();
  }

  @override
  void onInit() async {
    //await loadOrder();

    super.onInit();
  }

  loadOrder() async {
    setCarroProductosTemporal([]);
    GlobalController _globalController = Get.find();
    if ((_globalController.mesaSelected.isOcupada &&
            !_globalController.mesaSelected.isPagada &&
            !_globalController.mesaSelected.isImpresa) ||
        (_globalController.mesaSelected.isOcupada &&
            !_globalController.mesaSelected.isPagada &&
            _globalController.mesaSelected.isImpresa) ||
        (_globalController.mesaSelected.isOcupada &&
            _globalController.mesaSelected.isPagada &&
            _globalController.mesaSelected.isImpresa)) {
      EasyLoading.show(status: 'Cargando orden...', dismissOnTap: false);
      List<Orden> ordens = await OrdenesService.instance
          .obtenerOrdenPorMesa(_globalController.mesaSelected.id);

      setOrden(await OrdenesService.instance.obtenerOrdenPorId(ordens[0].id));
      setCarroProductosTemporal(_orden.carroProductos);
      int totalAcum = 0;
      _carroProductosTemporal.forEach((Producto producto) {
        totalAcum = totalAcum + (producto.precpred * producto.cantidad);
      });
      setTotal(totalAcum);

      EasyLoading.dismiss();
    } else {
      setTotal(0);
    }
  }

  Future<void> save() async {
    GlobalController _globalController = Get.find();
    HomeController _homeController = Get.find();
    TableController _tableController = Get.find();
    EasyLoading.show(status: 'Guardando orden...', dismissOnTap: false);

    if (_orden != null) {
      bool ordenPagada = await OrdenesService.instance
          .obtenerEstadoPagadoOrdenPorIdYSucursal(
              _orden.numOrden, _globalController.usuario.sucursalId);
      if (ordenPagada) {
        Get.snackbar(
          'Ya esta pagado!',
          'La orden ya esta pagada',
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red,
          colorText: Colors.white,
        );
        _carroProductosTemporal = [];
        setOrden(null);
        setTotal(0);
        setGavetaId(0);
        EasyLoading.dismiss();

        await _tableController.loadAmbientes();
        await _tableController.loadTables();

        _homeController.setIndexHome(1);
      } else {
        OrdenActualizarDto _ordenActualizarDto = OrdenActualizarDto();
        _ordenActualizarDto.id = _orden.id;
        _ordenActualizarDto.fecha = _orden.fecha;
        _ordenActualizarDto.hora = _orden.hora;
        _ordenActualizarDto.usuariosId = _orden.usuariosId;
        _ordenActualizarDto.usuarios = _orden.usuarios;
        _ordenActualizarDto.pagado = _orden.pagado;
        _ordenActualizarDto.propinaCant = _orden.propinaCant;
        _ordenActualizarDto.propinaDif = _orden.propinaCant;
        _ordenActualizarDto.descEfectivo = _orden.descEfectivo;
        _ordenActualizarDto.dctoOrden = _orden.dctoOrden;
        _ordenActualizarDto.dctoArt = _orden.dctoArt;
        _ordenActualizarDto.dctoEfec = _orden.dctoEfec;
        _ordenActualizarDto.total = _orden.total;
        _ordenActualizarDto.subtotal = _orden.subtotal;
        _ordenActualizarDto.urlDocFacturacion = _orden.urlDocFacturacion;
        _ordenActualizarDto.numFolio = _orden.numFolio;
        _ordenActualizarDto.idBsale = _orden.idBsale;
        _ordenActualizarDto.tedSii = _orden.tedSii;
        _ordenActualizarDto.tipoDocumentoSii = _orden.tipoDocumentoSii;
        _ordenActualizarDto.horaTermino = _orden.horaTermino;
        _ordenActualizarDto.fechaTermino = _orden.fechaTermino;
        _ordenActualizarDto.cajaId = _orden.cajaId;
        _ordenActualizarDto.caja = _orden.caja;
        _ordenActualizarDto.tiposVentaId = _orden.tiposVentaId;
        _ordenActualizarDto.tiposVenta = _orden.tiposVenta;
        _ordenActualizarDto.sucursalId = _orden.sucursalId;
        _ordenActualizarDto.sucursal = _orden.sucursal;
        _ordenActualizarDto.detalleDescuentoId = _orden.detalleDescuentoId;
        _ordenActualizarDto.detalleDescuento = _orden.detalleDescuento;
        _ordenActualizarDto.cierreCajaId = _orden.cierreCajaId;
        _ordenActualizarDto.cierreCaja = _orden.cierreCaja;
        _ordenActualizarDto.gavetaId = _orden.gavetaId;
        _ordenActualizarDto.gaveta = _orden.gaveta;
        _ordenActualizarDto.clienteId = _orden.clienteId;
        _ordenActualizarDto.cliente = _orden.cliente;
        _ordenActualizarDto.infoDeliveryId = _orden.infoDeliveryId;
        _ordenActualizarDto.infoDelivery = _orden.infoDelivery;
        _ordenActualizarDto.mediosPorOrdens = _orden.mediosPorOrdens;
        _ordenActualizarDto.productosOrdens = _orden.productosOrdens;
        _ordenActualizarDto.isAnulada = _orden.isAnulada;
        _ordenActualizarDto.motivoAnulacion = _orden.motivoAnulacion;
        _ordenActualizarDto.carroProductos = _carroProductosTemporal;
        _ordenActualizarDto.carroProductosDos = _orden.carroProductosDos;
        _ordenActualizarDto.isPendiente = _orden.isPendiente;
        _ordenActualizarDto.mesaId = _orden.mesaId;
        _ordenActualizarDto.mesa = _orden.mesa;
        _ordenActualizarDto.propinaPorc = _orden.propinaPorc;
        _ordenActualizarDto.numeroDeClientes =
            _numeroClientes != null ? _numeroClientes : _orden.numeroDeClientes;
        _ordenActualizarDto.nombreCliente =
            _nombreCliente != null ? _nombreCliente : _orden.nombreCliente;
        _ordenActualizarDto.isImpresa = _orden.isImpresa;
        _ordenActualizarDto.numOrden = _orden.numOrden;
        _ordenActualizarDto.isListo = _orden.isListo;
        _ordenActualizarDto.fechaOrdenLista = _orden.fechaOrdenLista;
        _ordenActualizarDto.jsonFacturacionSii = _orden.jsonFacturacionSii;
        _ordenActualizarDto.jsonRespBsale = _orden.jsonRespBsale;
        _ordenActualizarDto.fechaFacturacion = _orden.fechaFacturacion;
        _ordenActualizarDto.pagadoDelivery = _orden.pagadoDelivery;
        _ordenActualizarDto.listaMediosPago = _orden.listaMediosPago;
        _ordenActualizarDto.minutosDesdeApertura = _orden.minutosDesdeApertura;
        _ordenActualizarDto.idPedidoDelivery = _orden.idPedidoDelivery;
        _ordenActualizarDto.ID = _orden.id;
        _ordenActualizarDto.Pagado = false;
        _ordenActualizarDto.TiposVentaID = 1;
        _ordenActualizarDto.MesaID = _orden.mesaId;
        _ordenActualizarDto.DescEfectivo = _orden.descEfectivo;
        _ordenActualizarDto.DctoOrden = _orden.dctoOrden;
        _ordenActualizarDto.DctoArt = _orden.dctoArt;
        _ordenActualizarDto.Total = this.getTotal();
        _ordenActualizarDto.Subtotal = _total;
        _ordenActualizarDto.CajaID = _globalController.usuario.cajaId;
        _ordenActualizarDto.SucursalID = _globalController.usuario.sucursalId;
        await OrdenesService.instance
            .actualizarOrden(_ordenActualizarDto.ID, _ordenActualizarDto);
        // setNombreCliente(null);
        // setNumeroClientes(null);

        // _globalController.clearMesa();

        // Get.offAll(LoginPage());
        await _tableController.loadAmbientes();
        await _tableController.loadTables();
        Mesa mesa = _tableController.mesas.firstWhere(
            (element) => element.id == _globalController.mesaSelected.id);
        _globalController.setMesaSelected(mesa);
        if (_globalController.usuario.sucursal.configPos.cloudPrintActive) {
          await OrdenesService.instance
              .sendToPrintCloud(_ordenActualizarDto.id);
        }
        await loadOrder();
        EasyLoading.dismiss();

        _homeController.setIndexHome(1);
      }
    } else {
      OrdenCrearDto _ordenCrearDto = OrdenCrearDto();
      _ordenCrearDto.pagado = false;
      _ordenCrearDto.tiposVentaId = 1;
      _ordenCrearDto.mesaId = _globalController.mesaSelected.id;
      _ordenCrearDto.descEfectivo = 0;
      _ordenCrearDto.numeroDeClientes = _numeroClientes != null
          ? _numeroClientes
          : _orden == null
              ? 1
              : _orden.numeroDeClientes;
      _ordenCrearDto.nombreCliente = _nombreCliente != null
          ? _nombreCliente
          : _orden == null
              ? null
              : _orden.nombreCliente;
      _ordenCrearDto.descuentoPorcentaje = 0;
      _ordenCrearDto.carroProductos = _carroProductosTemporal;
      _ordenCrearDto.total = _total;
      _ordenCrearDto.subtotal = _total;
      _ordenCrearDto.usuariosId = _globalController.usuario.id;
      _ordenCrearDto.cajaId = _globalController.usuario.cajaId;
      _ordenCrearDto.sucursalId = _globalController.usuario.sucursalId;

      Orden ordenCreate =
          await OrdenesService.instance.crearOrden(_ordenCrearDto);
      // setNombreCliente(null);
      // setNumeroClientes(null);

      await _tableController.loadAmbientes();
      await _tableController.loadTables();

      Mesa mesa = _tableController.mesas.firstWhere(
          (element) => element.id == _globalController.mesaSelected.id);
      _globalController.setMesaSelected(mesa);
      if (_globalController.usuario.sucursal.configPos.cloudPrintActive) {
        await OrdenesService.instance.sendToPrintCloud(ordenCreate.id);
      }

      await loadOrder();
      EasyLoading.dismiss();

      // _globalController.clearMesa();
      // Get.offAll(LoginPage());

      _homeController.setIndexHome(1);
    }
  }

  int getDescuento() {
    var cont = 0.0;
    if (_orden.dctoOrden > 0) {
      cont += _total * (_orden.dctoOrden / 100);
    }
    if (_orden.descEfectivo > 0) {
      cont += _orden.descEfectivo;
    }
    if (_carroProductosTemporal.length > 0) {
      _carroProductosTemporal.forEach((producto) {
        if (producto.descuentoPorc > 0) {
          var descuento = (producto.precpred * producto.cantidad) *
              (producto.descuentoPorc / 100);
          cont += descuento;
        }
      });
    }
    return cont.round();
  }

  int getTotal() {
    var valorDescuento = this.getDescuento();
    var total = _total;

    return (total - valorDescuento).round();
  }

  void substractProduct(int index) {
    if (_carroProductosTemporal[index].cantidad == 1) {
      _carroProductosTemporal.removeAt(index);
    } else {
      _carroProductosTemporal[index].cantidad =
          _carroProductosTemporal[index].cantidad - 1;
    }

    int totalAcum = 0;
    _carroProductosTemporal.forEach((Producto producto) {
      totalAcum = totalAcum + (producto.precpred * producto.cantidad);
    });
    setTotal(totalAcum);

    update();
  }

  void deleteProduct(int index) {
    _carroProductosTemporal.removeAt(index);

    int totalAcum = 0;
    _carroProductosTemporal.forEach((Producto producto) {
      totalAcum = totalAcum + (producto.precpred * producto.cantidad);
    });
    setTotal(totalAcum);

    update();
  }

  void aumentProduct(int index) {
    _carroProductosTemporal[index].cantidad =
        _carroProductosTemporal[index].cantidad + 1;

    int totalAcum = 0;
    _carroProductosTemporal.forEach((Producto producto) {
      totalAcum = totalAcum + (producto.precpred * producto.cantidad);
    });
    setTotal(totalAcum);

    update();
  }

  Future<void> loadModificadores(Producto producto) async {
    EasyLoading.show(status: 'Cargando modificadores...', dismissOnTap: false);

    List<Modificador> modificadores =
        await ModificadorService.instance.obtenerModificadores();

    List<Modificador> newModificadores = [];

    producto.modificadores.forEach((modificador) {
      if (modificadores
          .where((element) => element.desmod == modificador)
          .toList()
          .isEmpty) {
        newModificadores
            .add(Modificador(desmod: modificador, isProducto: false));
      }
    });
    modificadores.addAll(newModificadores);
    setModificadores(modificadores);
    EasyLoading.dismiss();
  }
}
