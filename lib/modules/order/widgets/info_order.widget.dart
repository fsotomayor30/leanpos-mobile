import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/order/pages/order.page.dart';
import 'package:leanpos/modules/order/widgets/client_forn.widget.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class InfoOrderWidget extends StatefulWidget {
  @override
  _InfoOrderWidgetState createState() => _InfoOrderWidgetState();
}

class _InfoOrderWidgetState extends State<InfoOrderWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: GetBuilder(
          init: GlobalController(),
          builder: (GlobalController _) => GetBuilder(
              init: OrderController(),
              builder: (OrderController __) => GestureDetector(
                    onTap: () {
                      HomeController _homeController = Get.find();
                      _homeController.setContentHome(
                          actionIconActive: false,
                          contentHome: ClientFormWidget(),
                          backIconActive: true,
                          backFunction: () => _homeController.setContentHome(
                              contentHome: OrderPage(),
                              backIconActive: false,
                              actionIconActive: true,
                              actionFunction: () async => await __.save(),
                              actionIcon: FaIcon(
                                Icons.check_circle,
                                size: 30.0,
                                color: Colors.green,
                              )));
                    },
                    child: Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        color: Color(0xFF59978c),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      'Mesero: ${_.usuario.nombre} ${_.usuario.apellido}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12)),
                                  Text('Estación: ${_.usuario.cajaId}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12)),
                                  Text(
                                      'N° Orden: ${__.orden == null ? 'Nueva Orden' : __.orden.numOrden}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12)),
                                  Text(
                                      'Nombre: ${__.orden == null ? '-' : __.orden.nombreCliente == null ? __.nombreCliente == null ? '-' : __.nombreCliente : __.orden.nombreCliente.toString()}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12)),
                                  Text(
                                      'Clientes: ${__.orden == null ? '1' : __.orden.numeroDeClientes.toString()}',
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 12))
                                ]),
                          ),
                          SizedBox(width: 20),
                          FaIcon(
                            FontAwesomeIcons.ellipsisV,
                            color: Colors.white,
                            size: 15,
                          )
                        ],
                      ),
                    ),
                  ))),
    );
  }
}
