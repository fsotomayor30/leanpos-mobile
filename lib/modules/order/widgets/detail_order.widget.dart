import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/order/pages/modifies.page.dart';
import 'package:leanpos/modules/order/pages/order.page.dart';
import 'package:leanpos/shared/utlils/number.formatter.dart';

class DetailOrderWidget extends StatefulWidget {
  @override
  _DetailOrderWidgetState createState() => _DetailOrderWidgetState();
}

class _DetailOrderWidgetState extends State<DetailOrderWidget> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: GetBuilder(
              init: OrderController(),
              builder: (OrderController orderController) => ListView.builder(
                  itemCount: orderController.carroProductosTemporal.length,
                  itemBuilder: (BuildContext ctxt, int index) {
                    return Slidable(
                      actionPane: SlidableDrawerActionPane(),
                      actionExtentRatio: 0.25,
                      child: Container(
                        child: ListTile(
                          onTap: () {
                            if (!orderController
                                .carroProductosTemporal[index].isComanda) {
                              Get.snackbar(
                                'En preparación',
                                'Este producto ya lo estan preparando, contacta al administrador',
                                snackPosition: SnackPosition.BOTTOM,
                                backgroundColor: Colors.red,
                                colorText: Colors.white,
                              );
                            }
                          },
                          leading: CircleAvatar(
                            backgroundColor: Color(0xFF59978c),
                            child: Text(orderController
                                .carroProductosTemporal[index].cantidad
                                .toString()),
                            foregroundColor: Colors.white,
                          ),
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(orderController
                                  .carroProductosTemporal[index].desprod),
                              Column(
                                children: orderController
                                    .carroProductosTemporal[index].modificadores
                                    .map((modificador) => Row(
                                          children: [
                                            FaIcon(
                                              FontAwesomeIcons.exclamation,
                                              color: Colors.red,
                                              size: 12,
                                            ),
                                            SizedBox(width: 10),
                                            Flexible(
                                              child: Text(modificador,
                                                  maxLines: 1,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.grey)),
                                            ),
                                          ],
                                        ))
                                    .toList(),
                              )
                            ],
                          ),
                          subtitle: Text(
                              '${moneyFormatter(orderController.carroProductosTemporal[index].precpred * orderController.carroProductosTemporal[index].cantidad)}'),
                        ),
                      ),
                      actions: getOpcions(
                          orderController.carroProductosTemporal[index], index),
                      secondaryActions: getOpcionsSecondary(
                          orderController.carroProductosTemporal[index], index),
                    );
                  })),
        ),
      ),
    );
  }

  List<Widget> getOpcions(Producto product, int index) {
    OrderController _orderController = Get.find();
    HomeController _homeController = Get.find();

    List<Widget> opciones = [];
    if (product.isComanda) {
      opciones.add(IconSlideAction(
        caption: 'Aumentar',
        color: Colors.green,
        icon: Icons.exposure_plus_1,
        onTap: () => _orderController.aumentProduct(index),
      ));

      opciones.add(IconSlideAction(
        caption: 'Dismunuir',
        color: Colors.red,
        icon: Icons.exposure_minus_1,
        onTap: () => _orderController.substractProduct(index),
      ));

      if (product.tipomodif == 0) {
        opciones.add(IconSlideAction(
            caption: 'Mensaje',
            color: Color(0xFF59978c),
            icon: Icons.message,
            onTap: () {
              _orderController.loadModificadores(product);
              _homeController.setContentHome(
                  actionIconActive: true,
                  actionIcon: FaIcon(
                    Icons.check_circle,
                    size: 30.0,
                    color: Colors.green,
                  ),
                  actionFunction: () => _homeController.setContentHome(
                      contentHome: OrderPage(),
                      backIconActive: false,
                      actionIconActive: true,
                      actionFunction: () async => await _orderController.save(),
                      actionIcon: FaIcon(
                        Icons.check_circle,
                        size: 30.0,
                        color: Colors.green,
                      )),
                  contentHome: ModifierPage(
                    producto: product,
                    indice: index,
                  ),
                  backIconActive: true,
                  backFunction: () => _homeController.setContentHome(
                      contentHome: OrderPage(),
                      backIconActive: false,
                      actionIconActive: true,
                      actionFunction: () async => await _orderController.save(),
                      actionIcon: FaIcon(
                        Icons.check_circle,
                        size: 30.0,
                        color: Colors.green,
                      )));
            }));
      }
    }

    return opciones;
  }

  List<Widget> getOpcionsSecondary(Producto product, int index) {
    OrderController _orderController = Get.find();

    List<Widget> opciones = [];
    if (product.isComanda) {
      opciones.add(IconSlideAction(
        caption: 'Eliminar',
        color: Colors.red,
        icon: Icons.delete,
        onTap: () => _orderController.deleteProduct(index),
      ));
    }

    return opciones;
  }
}
