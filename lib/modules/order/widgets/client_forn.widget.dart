import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/shared/widgets/input_text.widget.dart';

class ClientFormWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: OrderController(),
        builder: (OrderController _) => ListView(
              children: [
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: InputTextWidget(
                    initValue: _.numeroClientes != null
                        ? _.numeroClientes.toString()
                        : _.orden == null
                            ? '1'
                            : _.orden.numeroDeClientes.toString(),
                    onChanged: (String value) {
                      if (value.length == 0) {
                        _.setNumeroClientes(1);
                      } else {
                        _.setNumeroClientes(int.parse(value));
                      }
                    },
                    obscureText: false,
                    hint: 'Ingrese numero de clientes',
                    keyboardType: TextInputType.number,
                    onlyNumber: true,
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: InputTextWidget(
                      initValue: _.nombreCliente != null
                          ? _.nombreCliente.toString()
                          : _.orden == null
                              ? null
                              : _.orden.nombreCliente,
                      onChanged: (String value) {
                        if (value.length == 0) {
                          _.setNombreCliente(null);
                        } else {
                          _.setNombreCliente(value);
                        }
                      },
                      obscureText: false,
                      hint: 'Ingrese nombre del cliente',
                      keyboardType: TextInputType.text),
                )
              ],
            ));
  }
}
