import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/shared/utlils/number.formatter.dart';

class TotalOrderWidget extends StatefulWidget {
  @override
  _TotalOrderWidgetState createState() => _TotalOrderWidgetState();
}

class _TotalOrderWidgetState extends State<TotalOrderWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: GetBuilder(
          init: OrderController(),
          builder: (OrderController _) =>  Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
              width: Get.width,
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color(0xFF59978c),
                  //color: Colors.white,
                  //border: Border.all(width: 1,color: Color(0xFF707070),style: BorderStyle.solid)

              ),
              child: Text('Total: '+moneyFormatter(_.total), textAlign: TextAlign.center, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),),
            ),
          )),
    );
  }
}
