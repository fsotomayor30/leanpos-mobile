import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/order/widgets/detail_order.widget.dart';
import 'package:leanpos/modules/order/widgets/info_order.widget.dart';
import 'package:leanpos/modules/order/widgets/total_order.widget.dart';

class OrderPage extends StatefulWidget {
  @override
  _OrderPageState createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: OrderController(),
        builder: (OrderController _) => Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 5),
              child: Column(
                children: [
                  InfoOrderWidget(),
                  DetailOrderWidget(),
                  TotalOrderWidget()
                ],
              ),
            ));
  }
}
