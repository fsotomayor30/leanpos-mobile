import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';

class ModifierPage extends StatelessWidget {
  final Producto producto;
  final int indice;
  ModifierPage({@required this.producto, @required this.indice});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: OrderController(),
        builder: (OrderController orderController) => Column(
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            orderController
                                .setExpanded(!orderController.expanded);
                          },
                          child: Row(children: [
                            Expanded(
                                child: Text(
                              'Escribir modificador',
                              style:
                                  TextStyle(fontSize: 16, color: Colors.black),
                            )),
                            FaIcon(
                                orderController.expanded
                                    ? FontAwesomeIcons.sortDown
                                    : FontAwesomeIcons.sortUp,
                                color: Color(0xFF707070))
                          ]),
                        ),
                        orderController.expanded
                            ? TextFormField(
                                controller:
                                    orderController.textModifierController,
                                decoration:
                                    InputDecoration(labelText: 'Modificador'),
                              )
                            : Container(),
                        orderController.expanded
                            ? Align(
                                alignment: Alignment.centerRight,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    primary: Color(0xFF59978c),
                                  ),
                                  child: Text('Confirmar'),
                                  onPressed: () {
                                    orderController.agregarModificadorTexto(
                                        indiceProducto: indice);
                                  },
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: ListView(
                    children: orderController.modificadores
                        .where((modificador) =>
                            !modificador.isProducto &&
                            modificador.relProdModId == null)
                        .map((modificadorMap) => Card(
                              color: producto.modificadores
                                      .where((element) =>
                                          element == modificadorMap.desmod)
                                      .toList()
                                      .isNotEmpty
                                  ? Colors.green
                                  : Colors.white,
                              child: ListTile(
                                onTap: () => producto.modificadores
                                        .where((element) =>
                                            element == modificadorMap.desmod)
                                        .toList()
                                        .isNotEmpty
                                    ? orderController.eliminarModificador(
                                        indiceProducto: indice,
                                        modificador: modificadorMap.desmod)
                                    : orderController.agregarModificador(
                                        indiceProducto: indice,
                                        modificador: modificadorMap.desmod),
                                title: Text(modificadorMap.desmod,
                                    style: TextStyle(
                                        color: producto.modificadores
                                                .where((element) =>
                                                    element ==
                                                    modificadorMap.desmod)
                                                .toList()
                                                .isNotEmpty
                                            ? Colors.white
                                            : Colors.black)),
                              ),
                            ))
                        .toList(),
                  ),
                ),
              ],
            ));
  }
}
