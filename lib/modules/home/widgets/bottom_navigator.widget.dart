
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';

class BottomNavigatorWidget extends StatefulWidget {
  @override
  _BottomNavigatorWidgetState createState() => _BottomNavigatorWidgetState();
}

class _BottomNavigatorWidgetState extends State<BottomNavigatorWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: HomeController(),
        builder: (_) => BottomNavigationBar(
          onTap: (int index){
            _.setIndexHome(index);
          },
          currentIndex: _.indexHome,
          items: [
            BottomNavigationBarItem(
                icon: FaIcon(FontAwesomeIcons.thLarge, color: Color(0xFF707070)),
                label: ''
            ),
            BottomNavigationBarItem(
              icon: FaIcon(FontAwesomeIcons.chair, color: Color(0xFF707070)),
                label: ''

            ),
            BottomNavigationBarItem(
                icon: Stack(
                  children: [
                    FaIcon(FontAwesomeIcons.receipt, color: Color(0xFF707070)),
                GetBuilder(
                  init: OrderController(),
                  builder: (OrderController __) => __.carroProductosTemporal.length == 0 ? Container(width: 0, height: 0,) : Positioned(
                        top: 0,
                        right: 0,
                        child: Container(
                          transform: Matrix4.translationValues(5.0, 15, 0.0),
                          width: 15,
                          height: 15,
                          decoration: new BoxDecoration(
                              color: Colors.red,
                              borderRadius: new BorderRadius.all(
                                  const Radius.circular(15.0)
                              )
                          ),
                          child: Center(child: Text(__.carroProductosTemporal.length.toString(), textAlign: TextAlign.center, style: TextStyle(fontSize: 10, color: Colors.white))),
                        )
                    )),
                  ],
                ),
                label: ''
            ),
          ],
          type: BottomNavigationBarType.fixed,
        ),

    );
  }
}
