import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/home/widgets/bottom_navigator.widget.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/table/controllers/table.controller.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';
import 'package:leanpos/shared/widgets/drawer.widget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: HomeController(),
        builder: (HomeController _) => Scaffold(
              key: _.scaffoldKey,
              appBar: AppBar(
                backgroundColor: Color(0xFFFFFFFF),
                centerTitle: true,
                title: Image.asset('assets/leanpos.png', height: 40),
                leading: _.backIconActive
                    ? GestureDetector(
                        onTap: () => _.backFunction(),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10, left: 15),
                          child: FaIcon(
                            FontAwesomeIcons.arrowLeft,
                            color: Color(0xFF59978c),
                          ),
                        ),
                      )
                    : GestureDetector(
                        onTap: () {
                          _.openDrawer();
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10, left: 15),
                          child: FaIcon(
                            FontAwesomeIcons.bars,
                            color: Color(0xFF59978c),
                          ),
                        )),
                actions: <Widget>[
                  _.actionIconActive
                      ? GestureDetector(
                          onTap: () async => await _.actionFunction(),
                          child: Padding(
                            padding: EdgeInsets.only(right: 20.0, top: 12),
                            child: _.actionIcon,
                          ),
                        )
                      : Container()
                  // _.indexHome == 1
                  //     ? GetBuilder(
                  //         init: TableController(),
                  //         builder: (TableController __) => Padding(
                  //             padding: EdgeInsets.only(right: 20.0),
                  //             child: GestureDetector(
                  //               onTap: () async => await __.loadTables(),
                  //               child: Icon(
                  //                 Icons.refresh,
                  //                 size: 30.0,
                  //                 color: Colors.green,
                  //               ),
                  //             )))
                  //     : _.indexHome == 2
                  //         ? GetBuilder(
                  //             init: OrderController(),
                  //             builder: (OrderController __) => __.loadingSave
                  //                 ? Padding(
                  //                     padding: EdgeInsets.only(right: 20.0),
                  //                     child: Center(
                  //                         child: CircularProgressIndicator()),
                  //                   )
                  //                 : Padding(
                  //                     padding: EdgeInsets.only(right: 20.0),
                  //                     child: GestureDetector(
                  //                       onTap: () async => await __.save(),
                  //                       child: Icon(
                  //                         Icons.check_circle,
                  //                         size: 30.0,
                  //                         color: Colors.green,
                  //                       ),
                  //                     )))
                  //         : Container(),
                ],
              ),
              drawer: DrawerWidget(),
              body: Column(
                children: [
                  Expanded(
                    child: _.contentHome,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  GetBuilder(
                      init: GlobalController(),
                      builder: (GlobalController _) => Container(
                            width: Get.width,
                            height: 50,
                            color: _.mesaSelected == null
                                ? Colors.red
                                : Color(0xFF59978c),
                            child: Center(
                              child: _.mesaSelected == null
                                  ? Text(
                                      'Sin mesa seleccionada',
                                      style: TextStyle(color: Colors.white),
                                    )
                                  : Text(
                                      'Mesa seleccionada: ${_.mesaSelected.descipcion}',
                                      style: TextStyle(color: Colors.white),
                                    ),
                            ),
                          ))
                ],
              ),
              bottomNavigationBar: BottomNavigatorWidget(),
            ));
  }
}
