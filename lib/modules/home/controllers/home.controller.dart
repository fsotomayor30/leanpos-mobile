import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/order/pages/order.page.dart';
import 'package:leanpos/modules/product/pages/product.page.dart';
import 'package:leanpos/modules/table/controllers/table.controller.dart';
import 'package:leanpos/modules/table/pages/table.page.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class HomeController extends GetxController {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<ScaffoldState> get scaffoldKey => _scaffoldKey;

  bool _expanded = true;
  bool get expanded => _expanded;

  int _indexHome = 0;
  int get indexHome => _indexHome;

  Widget _contentHome = Container();
  Widget get contentHome => _contentHome;

  bool _backIconActive = false;
  bool get backIconActive => _backIconActive;

  Function _backFunction;
  Function get backFunction => _backFunction;

  bool _actionIconActive = false;
  bool get actionIconActive => _actionIconActive;

  Function _actionFunction;
  Function get actionFunction => _actionFunction;

  FaIcon _actionIcon;
  FaIcon get actionIcon => _actionIcon;

  void setBackIconActive(bool backIconActive) {
    _backIconActive = backIconActive;
    update();
  }

  void setBackFunction(Function backFunction) {
    _backFunction = backFunction;
    update();
  }

  void setActionIconActive(bool actionIconActive) {
    _actionIconActive = actionIconActive;
    update();
  }

  void setActionIcon(FaIcon actionIcon) {
    _actionIcon = actionIcon;
    update();
  }

  void setActionFunction(Function actionFunction) {
    _actionFunction = actionFunction;
    update();
  }

  void setExpanded(bool expanded) {
    _expanded = expanded;
    update();
  }

  void openDrawer() {
    _scaffoldKey.currentState.openDrawer();
  }

  void setIndexHome(int indexHome) {
    GlobalController _globalController = Get.find();

    switch (indexHome) {
      case 0:
        if (_globalController.mesaSelected != null) {
          _indexHome = indexHome;
          setContentHome(
              contentHome: ProductPage(),
              backIconActive: false,
              actionIconActive: false);
        } else {
          _indexHome = 1;
          TableController tableController = Get.find();
          setContentHome(
              contentHome: TablePage(),
              backIconActive: false,
              actionIconActive: true,
              actionFunction: () async => await tableController.loadTables(),
              actionIcon: FaIcon(
                FontAwesomeIcons.syncAlt,
                size: 30.0,
                color: Colors.green,
              ));
        }
        break;
      case 1:
        _indexHome = indexHome;
        Get.put(TableController());
        TableController tableController = Get.find();

        setContentHome(
            contentHome: TablePage(),
            backIconActive: false,
            actionIconActive: true,
            actionFunction: () async => await tableController.loadTables(),
            actionIcon: FaIcon(
              FontAwesomeIcons.syncAlt,
              size: 30.0,
              color: Colors.green,
            ));
        break;
      case 2:
        if (_globalController.mesaSelected != null) {
          _indexHome = indexHome;
          OrderController orderController = Get.find();
          setContentHome(
              contentHome: OrderPage(),
              backIconActive: false,
              actionIconActive: true,
              actionFunction: () async => await orderController.save(),
              actionIcon: FaIcon(
                Icons.check_circle,
                size: 30.0,
                color: Colors.green,
              ));
        } else {
          _indexHome = 1;
          TableController tableController = Get.find();

          setContentHome(
              contentHome: TablePage(),
              backIconActive: false,
              actionIconActive: true,
              actionFunction: () async => await tableController.loadTables(),
              actionIcon: FaIcon(
                FontAwesomeIcons.syncAlt,
                size: 30.0,
                color: Colors.green,
              ));
        }
        break;
    }
    update();
  }

  void setContentHome(
      {@required Widget contentHome,
      @required bool backIconActive,
      Function backFunction,
      @required bool actionIconActive,
      Function actionFunction,
      FaIcon actionIcon}) {
    _contentHome = contentHome;
    setBackIconActive(backIconActive);
    if (backFunction != null) {
      setBackFunction(backFunction);
    }

    setActionIconActive(actionIconActive);
    if (actionFunction != null) {
      setActionIcon(actionIcon);
      setActionFunction(actionFunction);
    }
    update();
  }

  @override
  void onInit() {
    GlobalController _globalController = Get.find();
    if (_globalController.mesaSelected != null) {
      setIndexHome(0);
    } else {
      setIndexHome(1);
    }

    super.onInit();
  }
}
