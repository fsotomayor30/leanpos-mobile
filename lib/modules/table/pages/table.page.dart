import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/table/controllers/table.controller.dart';
import 'package:leanpos/modules/table/widgets/ambiente_item.widget.dart';
import 'package:leanpos/modules/table/widgets/table_item.widget.dart';

class TablePage extends StatefulWidget {
  @override
  _TablePageState createState() => _TablePageState();
}

class _TablePageState extends State<TablePage> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: TableController(),
        builder: (TableController _) => Scaffold(
              body: Container(
                  padding: const EdgeInsets.only(top: 0),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: Container(
                          child: Column(
                            children: [
                              Container(
                                height: 40,
                                child: ListView.builder(
                                    padding: EdgeInsets.only(left: 10),
                                    scrollDirection: Axis.horizontal,
                                    itemCount: _.ambientes.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return AmbienteItemWidget(
                                          ambiente: _.ambientes[index]);
                                    }),
                              ),
                              SizedBox(height: 10)
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: GridView.builder(
                            itemCount: _.mesas
                                .where((element) =>
                                    element.ambienteId == _.filter ||
                                    _.filter == -1)
                                .length,
                            padding: EdgeInsets.symmetric(
                                horizontal: 10, vertical: 10),
                            shrinkWrap: false,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    crossAxisSpacing: 5,
                                    childAspectRatio: 2,
                                    mainAxisSpacing: 5),
                            itemBuilder: (BuildContext context, int index) {
                              if (_.filter ==
                                      _.mesas
                                          .where((element) =>
                                              element.ambienteId == _.filter ||
                                              _.filter == -1)
                                          .toList()[index]
                                          .ambienteId ||
                                  _.filter == -1) {
                                return TableItemWidget(
                                    mesa: _.mesas
                                        .where((element) =>
                                            element.ambienteId == _.filter ||
                                            _.filter == -1)
                                        .toList()[index]);
                              } else {
                                return Container(
                                  width: 0,
                                  height: 0,
                                );
                              }
                            }),
                      ),
                    ],
                  )),
            ));
  }
}
