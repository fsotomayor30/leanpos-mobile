import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/ambiente.entity.dart';
import 'package:leanpos/entities/mesa.entity.dart';
import 'package:leanpos/http/mesas.service.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class TableController extends GetxController {
  int _filter = -1;
  int get filter => _filter;

  List<Mesa> _mesas = [];
  List<Mesa> get mesas => _mesas;

  List<Ambiente> _ambientes = [Ambiente(descipcion: 'TODOS', id: -1)];
  List<Ambiente> get ambientes => _ambientes;

  void setFilter(int filter) {
    _filter = filter;
    update();
  }

  void setMesas(List<Mesa> mesas) {
    _mesas = mesas;
    update();
  }

  void setAmbientes(List<Ambiente> ambientes) {
    _ambientes.addAll(ambientes);
    update();
  }

  @override
  void onInit() async {
    await loadAmbientes();
    await loadTables();
    super.onInit();
  }

  loadTables() async {
    EasyLoading.show(status: 'Cargando mesas...', dismissOnTap: false);

    GlobalController _globalController = Get.find();
    List<Mesa> mesas = await MesasService.instance
        .obtenerMesaPorSucursal(_globalController.usuario.sucursalId);
    setMesas(mesas);
    EasyLoading.dismiss();
  }

  loadAmbientes() async {
    EasyLoading.show(status: 'Cargando ambientes...', dismissOnTap: false);
    GlobalController _globalController = Get.find();
    List<Ambiente> ambientes = await MesasService.instance
        .obtenerAmbientes(_globalController.usuario.sucursalId);
    setAmbientes(ambientes);
    EasyLoading.dismiss();
  }
}
