import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:leanpos/entities/ambiente.entity.dart';
import 'package:leanpos/modules/table/controllers/table.controller.dart';

class AmbienteItemWidget extends StatelessWidget {
  final Ambiente ambiente;

  AmbienteItemWidget({@required this.ambiente});

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: TableController(),
    builder: (TableController _) => GestureDetector(
      onTap: () => _.setFilter(ambiente.id),
      child: Padding(
        padding: const EdgeInsets.only(right: 3),
        child: Container(
          height: 40,
          width: 100,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(40)),
            color: _.filter == ambiente.id ? Color(0xFF59978c) : Colors.white,
            border: Border.all(
              color:  Color(0xFF59978c),
              width: 1.0,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text(ambiente.descipcion, maxLines: 1, overflow: TextOverflow.ellipsis, style: TextStyle(color: _.filter == ambiente.id ? Colors.white :  Color(0xFF59978c)),)),
          ),
        ),
      ),
    ));
  }
}
