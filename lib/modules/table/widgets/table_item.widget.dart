import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:leanpos/entities/mesa.entity.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class TableItemWidget extends StatefulWidget {
  final Mesa mesa;

  TableItemWidget({@required this.mesa});

  @override
  _TableItemWidgetState createState() => _TableItemWidgetState();
}

class _TableItemWidgetState extends State<TableItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
    builder: (GlobalController _) => GestureDetector(
    onTap: () => _.setMesaSelected(widget.mesa),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            color: (widget.mesa.isOcupada && !widget.mesa.isPagada && !widget.mesa.isImpresa) ? Color(0xFFBFC205) : (widget.mesa.isOcupada && !widget.mesa.isPagada && widget.mesa.isImpresa) ? Color(0xFFc740dc) : (widget.mesa.isOcupada && !widget.mesa.isPagada && widget.mesa.isImpresa) ? Color(0xFFc80303) : Color(0xFF529CE6)
        ),
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FaIcon(FontAwesomeIcons.utensils, color: Colors.white),
              SizedBox(width: 10),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                    border: Border.all(width: 4,color: Colors.white,style: BorderStyle.solid)
                ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                    child: Text(widget.mesa.descipcion, style: TextStyle(color: Colors.white, fontWeight:  FontWeight.bold, fontSize: 20),),
                  )
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
