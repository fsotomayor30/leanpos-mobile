import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/dto/promocion_por_producto.dto.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/http/productos.service.dart';
import 'package:leanpos/http/promociones.service.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/product/pages/forced_modifies.page.dart';
import 'package:leanpos/modules/product/pages/product.page.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class ProductController extends GetxController {
  PageController _pageController = PageController();
  PageController get pageController => _pageController;

  // Categoria _categorySelected;
  // Categoria get categorySelected => _categorySelected;

  // List<Categoria> _categories = [];
  // List<Categoria> get categories => _categories;

  // List<Producto> _products = [];
  // List<Producto> get products => _products;

  List<String> _forcedModifier = [];
  List<String> get forcedModifier => _forcedModifier;

  Producto _productForcedModifier = Producto();
  Producto get productForcedModifier => _productForcedModifier;

  int _indiceModif;
  int get indiceModif => _indiceModif;

  void setIndiceModif(int indiceModif) {
    _indiceModif = indiceModif;
    update();
  }

  // void setCategories(List<Categoria> categories) {
  //   _categories = categories;
  //   update();
  // }

  void setForcedModifier(List<String> forcedModifier) {
    _forcedModifier = forcedModifier;
    update();
  }

  // void setProduct(List<Producto> products) {
  //   _products = products;
  //   update();
  // }

  void setProductForcedModifier(Producto productForcedModifier) {
    _productForcedModifier = productForcedModifier;
    update();
  }

  // void setCategorySelected(Categoria categorySelected) {
  //   _categorySelected = categorySelected;
  //   update();
  // }

  @override
  void onInit() async {
    // EasyLoading.show(status: 'Cargando categorias...', dismissOnTap: false);

    // List<Categoria> categories = await CategoriasService.instance
    //     .obtenerCategorias(_globalController.usuario.sucursalId);
    // setCategories(categories);
    // EasyLoading.dismiss();

    // setCategorySelected(_categories[0]);

    // EasyLoading.show(status: 'Cargando productos...', dismissOnTap: false);
    // List<Producto> product = await ProductosService.instance.obtenerProductos(
    //     _globalController.usuario.sucursalId, _categorySelected.id);
    // setProduct(product);
    // EasyLoading.dismiss();
    super.onInit();
  }

  // Future<void> loadProduct(Categoria categoria) async {
  //   GlobalController _globalController = Get.find();

  //   setCategorySelected(categoria);
  //   EasyLoading.show(status: 'Cargando productos...', dismissOnTap: false);

  //   List<Producto> product = await ProductosService.instance.obtenerProductos(
  //       _globalController.usuario.sucursalId, _categorySelected.id);
  //   setProduct(product);
  //   EasyLoading.dismiss();
  // }

  Future<void> addProduct({Producto producto, bool isForceModifier}) async {
    EasyLoading.show(status: 'Agregando producto...', dismissOnTap: false);

    GlobalController _globalController = Get.find();
    OrderController _orderController = Get.find();
    HomeController _homeController = Get.find();
    Producto productFind = _orderController.carroProductosTemporal.firstWhere(
        (Producto element) =>
            (element.id == producto.id && element.isComanda == true),
        orElse: () => null);

    if (producto.tipomodif > 0 && !isForceModifier) {
      EasyLoading.dismiss();
      EasyLoading.show(
          status: 'Cargando modificadores forzados...', dismissOnTap: false);

      setIndiceModif(0);
      setForcedModifier([]);
      Producto productoForcedModifier = await ProductosService.instance
          .obtenerProductoConModificadores(producto.id);
      setProductForcedModifier(productoForcedModifier);
      EasyLoading.dismiss();

      _homeController.setContentHome(
          actionIconActive: false,
          actionFunction: () {},
          contentHome: ForcedModifierPage(),
          backIconActive: true,
          backFunction: () => _homeController.setContentHome(
                contentHome: ProductPage(),
                backIconActive: false,
                actionIconActive: false,
              ));
    } else {
      if (productFind == null) {
        Producto _productoTemporal = Producto();
        if (producto.isHabilitado) {
          _productoTemporal = producto;
          _productoTemporal.cantidad = 1;
          _productoTemporal.isCombo = producto.isCombo;
          _productoTemporal.codgrup = producto.tbgrupoId;
          _productoTemporal.id = producto.id;
          _productoTemporal.desprod = producto.desprod;
          _productoTemporal.precioFinal = producto.precioFinal;
          _productoTemporal.precpred = producto.precpred;
          _productoTemporal.tipomodif = producto.tipomodif;
          _productoTemporal.modificadores = _forcedModifier.isNotEmpty
              ? _forcedModifier
              : producto.modificadores;
          _productoTemporal.isGuardado = true;
          _productoTemporal.isPesable = producto.isPesable;
          _productoTemporal.isPesable = producto.isPesable;
          _productoTemporal.descuentoPorc = producto.descuentoPorc;
          _productoTemporal.isComanda = true;

          // if (_globalController.usuario.sucursal.configPos.isBarCode) {
          //   _productoTemporal.isComanda = true;
          // }

          if (_globalController.usuario.sucursal.configPos.isPromociones) {
            PromocionPorProductoDto _promocionPorProductoDto = await Promociones
                .instance
                .obtenerPromocionesPorProducto(producto.id);
            _productoTemporal.precpred = _productoTemporal.precpred +
                _promocionPorProductoDto.descuentoTotal;
          }
          List<Producto> _productosTemporales =
              _orderController.carroProductosTemporal;
          _productosTemporales.add(_productoTemporal);
          _orderController.setCarroProductosTemporal(_productosTemporales);
          int totalAcum = 0;
          _orderController.carroProductosTemporal.forEach((Producto producto) {
            totalAcum = totalAcum + (producto.precpred * producto.cantidad);
          });
          _orderController.setTotal(totalAcum);
          setForcedModifier([]);
          EasyLoading.dismiss();

          // Get.snackbar(
          //   'Éxito',
          //   'Has agregado el producto: ' + producto.desprod,
          //   snackPosition: SnackPosition.TOP,
          //   backgroundColor: Colors.grey[900],
          //   colorText: Colors.white,
          // );
          if (isForceModifier) {
            _homeController.setContentHome(
                contentHome: ProductPage(),
                backIconActive: false,
                actionIconActive: false);
          }
        }
      } else {
        int index =
            _orderController.carroProductosTemporal.indexOf(productFind);
        _orderController.aumentProduct(index);
        EasyLoading.dismiss();
        setForcedModifier([]);

        // Get.snackbar(
        //   'Éxito',
        //   'Has agregado el producto: ' + producto.desprod,
        //   snackPosition: SnackPosition.TOP,
        //   backgroundColor: Colors.grey[900],
        //   colorText: Colors.white,
        // );
        if (isForceModifier) {
          _homeController.setContentHome(
              contentHome: ProductPage(),
              backIconActive: false,
              actionIconActive: false);
        }
      }
    }
  }

  void forcedModifierClick(String forcedModifierItem) {
    if (_forcedModifier.contains(forcedModifierItem)) {
      _forcedModifier.remove(forcedModifierItem);
    } else {
      _forcedModifier.add(forcedModifierItem);
      if (_pageController.page.toInt() ==
          _productForcedModifier.relProdMods.length - 1) {
        addProduct(producto: _productForcedModifier, isForceModifier: true);
      } else {
        _pageController.animateToPage(_pageController.page.toInt() + 1,
            duration: Duration(milliseconds: 400), curve: Curves.easeIn);
      }
    }
    update();
  }
}
