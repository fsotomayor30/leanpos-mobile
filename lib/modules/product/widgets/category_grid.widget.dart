import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/product/controllers/product.controller.dart';
import 'package:leanpos/modules/product/widgets/category_item.widget.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class CategoryGridWidget extends StatefulWidget {
  @override
  _CategoryGridWidgetState createState() => _CategoryGridWidgetState();
}

class _CategoryGridWidgetState extends State<CategoryGridWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (GlobalController globalController) => Container(
            height: Get.height * .20,
            child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: GridView.builder(
                  itemCount: globalController.categories.length,
                  scrollDirection: Axis.horizontal,
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  shrinkWrap: false,
                  gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      crossAxisSpacing: 5,
                      mainAxisSpacing: 5,
                      childAspectRatio: (.4)),
                  itemBuilder: (BuildContext context, int index) {
                    return CategoryItemWidget(
                        category: globalController.categories[index]);
                  }),
            )));
  }
}
