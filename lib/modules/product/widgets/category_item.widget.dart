import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/categoria.entity.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class CategoryItemWidget extends StatefulWidget {
  final Categoria category;

  CategoryItemWidget({@required this.category});

  @override
  _CategoryItemWidgetState createState() => _CategoryItemWidgetState();
}

class _CategoryItemWidgetState extends State<CategoryItemWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (GlobalController globalController) => GestureDetector(
              onTap: () async {
                await globalController.loadProduct(widget.category);
              },
              child: Container(
                decoration: BoxDecoration(
                    color: globalController.categorySelected.id ==
                            widget.category.id
                        ? Color(0xFF59978c)
                        : Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(30)),
                    border: Border.all(
                        width: 1,
                        color: Color(0xFF59978c),
                        style: BorderStyle.solid)),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(widget.category.desgrup,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 12,
                            color: globalController.categorySelected.id ==
                                    widget.category.id
                                ? Colors.white
                                : Color(0xFF59978c))),
                  ),
                ),
              ),
            ));
  }
}
