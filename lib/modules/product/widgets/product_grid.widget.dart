import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/product/widgets/product_item.widget.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

import 'product_item.widget.dart';

class ProductGridWidget extends StatefulWidget {
  @override
  _ProductGridWidgetState createState() => _ProductGridWidgetState();
}

class _ProductGridWidgetState extends State<ProductGridWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: GlobalController(),
        builder: (GlobalController globalController) => Container(
                child: Padding(
              padding: const EdgeInsets.only(top: 15),
              child: GridView(
                padding: EdgeInsets.symmetric(horizontal: 10),
                shrinkWrap: false,
                gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 5,
                    childAspectRatio: (2)),
                children: globalController.products
                    .where((element) =>
                        element.tbgrupoId ==
                        globalController.categorySelected.id)
                    .toList()
                    .map((e) => ProductItemWidget(
                          product: e,
                        ))
                    .toList(),
              ),
            )));
  }
}
