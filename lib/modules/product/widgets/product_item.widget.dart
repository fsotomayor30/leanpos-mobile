import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/modules/product/controllers/product.controller.dart';

class ProductItemWidget extends StatefulWidget {
  final Producto product;

  ProductItemWidget({@required this.product});

  @override
  _ProductItemWidgetState createState() => _ProductItemWidgetState();
}

class _ProductItemWidgetState extends State<ProductItemWidget>
    with TickerProviderStateMixin {
  AnimationController _animationController;

  double _containerPaddingLeft = 20.0;
  double _animationValue;
  double _translateX = 0;
  double _translateY = 0;
  double _rotate = 0;
  double _scale = 1;

  bool show;
  bool sent = false;
  Color _color = Colors.white;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1300));
    show = true;
    _animationController.addListener(() {
      setState(() {
        show = false;
        _animationValue = _animationController.value;
        if (_animationValue >= 0.2 && _animationValue < 0.4) {
          _containerPaddingLeft = 100.0;
          _color = Color(0xFF59978c);
        } else if (_animationValue >= 0.4 && _animationValue <= 0.5) {
          _translateX = 80.0;
          _rotate = -20.0;
          _scale = 0.1;
        } else if (_animationValue >= 0.5 && _animationValue <= 0.8) {
          _translateY = -20.0;
        } else if (_animationValue >= 0.81) {
          _containerPaddingLeft = 20.0;
          sent = true;
        }
        if (_animationValue == 1.0) {
          _animationController.reset();

          show = true;
          sent = false;
          _color = Colors.white;
          _containerPaddingLeft = 20.0;
          _translateX = 0;
          _translateY = 0;
          _rotate = 0;
          _scale = 1;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: ProductController(),
        builder: (ProductController _) => GestureDetector(
            onTap: () async {
              _animationController.forward();
              await _.addProduct(
                  producto: widget.product, isForceModifier: false);
            },
            child: AnimatedContainer(
                decoration: BoxDecoration(
                  color: _color,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  border: Border.all(
                      width: 1,
                      color: Color(0xFF59978c),
                      style: BorderStyle.solid),
                ),
                padding: EdgeInsets.only(
                    left: _containerPaddingLeft,
                    right: 20.0,
                    top: 10.0,
                    bottom: 10.0),
                duration: Duration(milliseconds: 400),
                curve: Curves.easeOutCubic,
                child: Row(
                  // mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    (!sent)
                        ? AnimatedContainer(
                            duration: Duration(milliseconds: 400),
                            child: FaIcon(FontAwesomeIcons.utensils),
                            curve: Curves.fastOutSlowIn,
                            transform: Matrix4.translationValues(
                                _translateX, _translateY, 0)
                              ..rotateZ(_rotate)
                              ..scale(_scale),
                          )
                        : Container(),
                    AnimatedSize(
                      vsync: this,
                      duration: Duration(milliseconds: 600),
                      child: show ? SizedBox(width: 10.0) : Container(),
                    ),
                    show
                        ? Expanded(
                            child: AnimatedSize(
                                vsync: this,
                                duration: Duration(milliseconds: 200),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5, vertical: 5),
                                  child: Center(
                                    child: Text(
                                      widget.product.desprod,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style:
                                          TextStyle(color: Color(0xFF707070)),
                                    ),
                                  ),
                                )),
                          )
                        : Container(),
                    AnimatedSize(
                      vsync: this,
                      duration: Duration(milliseconds: 200),
                      child: sent
                          ? Icon(Icons.done, color: Colors.white)
                          : Container(),
                    ),
                    AnimatedSize(
                      vsync: this,
                      alignment: Alignment.topLeft,
                      duration: Duration(milliseconds: 600),
                      child: sent ? SizedBox(width: 10.0) : Container(),
                    ),
                    sent
                        ? Expanded(
                            child: AnimatedSize(
                                vsync: this,
                                duration: Duration(milliseconds: 200),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 5, vertical: 5),
                                  child: Center(
                                    child: Text(
                                      'Enviado',
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )),
                          )
                        : Container()
                  ],
                ))));
    // GestureDetector(
    //   onTap: () async => await _.addProduct(
    //       producto: widget.product, isForceModifier: false),
    //   child: Container(
    //     decoration: BoxDecoration(
    //         borderRadius: BorderRadius.all(Radius.circular(10)),
    //         border: Border.all(
    //             width: 1,
    //             color: Color(0xFF59978c),
    //             style: BorderStyle.solid)),
    //     child: Padding(
    //       padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
    //       child: Center(
    //         child: Text(
    //           widget.product.desprod,
    //           textAlign: TextAlign.center,
    //           maxLines: 2,
    //           overflow: TextOverflow.ellipsis,
    //           style: TextStyle(color: Color(0xFF707070)),
    //         ),
    //       ),
    //     ),
    //   ),
    // ),
  }
}
