import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/product/widgets/category_grid.widget.dart';

class CategoryWidget extends StatefulWidget {
  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
        init: HomeController(),
    builder: (_) => Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
         // border: Border.all(width: 1,color: Color(0xFF707070),style: BorderStyle.solid)
      ),
      child: Column(
        children: [
          GestureDetector(
            onTap: (){
              _.setExpanded(! _.expanded);
            },
            child: Row(
              children: [
                Expanded(child: Text('CATEGORIAS', textAlign: TextAlign.center, style: TextStyle(fontSize: 20, color: Color(0xFF707070)),)),
                FaIcon(_.expanded ? FontAwesomeIcons.sortDown :FontAwesomeIcons.sortUp ,color: Color(0xFF707070))
              ]
            ),
          ),
          _.expanded ? CategoryGridWidget() : Container()
        ],
      ),
    ));
  }
}
