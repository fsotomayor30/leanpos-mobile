import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/tb_modificadore.entity.dart';
import 'package:leanpos/modules/product/controllers/product.controller.dart';

class ForcedModifierPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductController>(
      builder: (ProductController productController) {
        return PageView(
          physics: new NeverScrollableScrollPhysics(),
          controller: productController.pageController,
          children: productController.productForcedModifier.relProdMods
              .map((e) => Column(
                    children: [
                      Expanded(
                        child: ListView(
                          children: e.tbModificadores
                              .map((TbModificadore tbModificadore) => Card(
                                    color: productController.forcedModifier
                                            .contains(tbModificadore.desmod)
                                        ? Color(0xFF59978c)
                                        : Colors.white,
                                    child: ListTile(
                                      title: Text(
                                        tbModificadore.desmod,
                                        style: TextStyle(
                                            color: productController
                                                    .forcedModifier
                                                    .contains(
                                                        tbModificadore.desmod)
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                      onTap: () {
                                        productController.forcedModifierClick(
                                            tbModificadore.desmod);
                                      },
                                    ),
                                  ))
                              .toList(),
                        ),
                      ),
                      if (e.isObligatorio)
                        Container()
                      else
                        GestureDetector(
                          onTap: () async => await productController.addProduct(
                              producto: productController.productForcedModifier,
                              isForceModifier: true),
                          child: Container(
                            color: Colors.red,
                            height: 50,
                            width: Get.width,
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Flexible(
                                    child: Text(
                                      'Selección no obligatoria, para confirmar el producto presiona aquí',
                                      maxLines: 2,
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                  SizedBox(width: 10),
                                  FaIcon(
                                    FontAwesomeIcons.times,
                                    color: Colors.white,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                    ],
                  ))
              .toList(),
        );
      },
    );
  }
}
