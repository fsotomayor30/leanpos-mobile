import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/product/controllers/product.controller.dart';
import 'package:leanpos/modules/product/widgets/category.widget.dart';
import 'package:leanpos/modules/product/widgets/product_grid.widget.dart';

class ProductPage extends StatefulWidget {
  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GetBuilder(
            init: ProductController(),
            builder: (ProductController _) => CategoryWidget()),
        GetBuilder(
            init: ProductController(),
            builder: (ProductController _) =>
                Expanded(child: ProductGridWidget()))
      ],
    );
  }
}
