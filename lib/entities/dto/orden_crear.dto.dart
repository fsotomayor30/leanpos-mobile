// To parse this JSON data, do
//
//     final ordenCrearDto = ordenCrearDtoFromJson(jsonString);

import 'dart:convert';

import 'package:leanpos/entities/producto.entity.dart';

OrdenCrearDto ordenCrearDtoFromJson(String str) => OrdenCrearDto.fromJson(json.decode(str));

String ordenCrearDtoToJson(OrdenCrearDto data) => json.encode(data.toJson());

class OrdenCrearDto {
  OrdenCrearDto({
    this.carroProductos,
    this.descEfectivo,
    this.numeroDeClientes,
    this.nombreCliente,
    this.descuentoPorcentaje,
    this.usuariosId,
    this.numCaja,
    this.pagado,
    this.tiposVentaId,
    this.mesaId,
    this.total,
    this.subtotal,
    this.cajaId,
    this.sucursalId,
  });

  List<Producto> carroProductos;
  int descEfectivo;
  int numeroDeClientes;
  int descuentoPorcentaje;
  int usuariosId;
  int numCaja;
  bool pagado;
  int tiposVentaId;
  int mesaId;
  int total;
  int subtotal;
  int cajaId;
  int sucursalId;
  String nombreCliente;

  factory OrdenCrearDto.fromJson(Map<String, dynamic> json) => OrdenCrearDto(
    carroProductos: List<Producto>.from(json["carroProductos"].map((x) => Producto.fromJson(x))),
    descEfectivo: json["descEfectivo"],
    numeroDeClientes: json["numeroDeClientes"],
    descuentoPorcentaje: json["descuentoPorcentaje"],
    usuariosId: json["usuariosID"],
    numCaja: json["numCaja"],
    pagado: json["Pagado"],
    tiposVentaId: json["TiposVentaID"],
    mesaId: json["MesaID"],
    total: json["Total"],
    subtotal: json["Subtotal"],
    cajaId: json["CajaID"],
    sucursalId: json["SucursalID"],
    nombreCliente: json["nombreCliente"],
  );

  Map<String, dynamic> toJson() => {
    "carroProductos": List<dynamic>.from(carroProductos.map((x) => x.toJson())),
    "descEfectivo": descEfectivo,
    "numeroDeClientes": numeroDeClientes,
    "descuentoPorcentaje": descuentoPorcentaje,
    "usuariosID": usuariosId,
    "numCaja": numCaja,
    "Pagado": pagado,
    "TiposVentaID": tiposVentaId,
    "MesaID": mesaId,
    "Total": total,
    "Subtotal": subtotal,
    "CajaID": cajaId,
    "SucursalID": sucursalId,
    "nombreCliente" : nombreCliente
  };
}

