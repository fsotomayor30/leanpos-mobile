import 'dart:convert';

PromocionPorProductoDto promocionPorProductoDtoFromJson(String str) => PromocionPorProductoDto.fromJson(json.decode(str));

String promocionPorProductoDtoToJson(PromocionPorProductoDto data) => json.encode(data.toJson());

class PromocionPorProductoDto {
  PromocionPorProductoDto({
    this.descuentoTotal,
  });

  int descuentoTotal;

  factory PromocionPorProductoDto.fromJson(Map<String, dynamic> json) => PromocionPorProductoDto(
    descuentoTotal: json["descuentoTotal"],
  );

  Map<String, dynamic> toJson() => {
    "descuentoTotal": descuentoTotal,
  };
}
