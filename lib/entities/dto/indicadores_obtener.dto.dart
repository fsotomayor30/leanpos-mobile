class IndicadoresObtenerDto {
  IndicadoresObtenerDto({
    this.fechaIni,
    this.tipoComparativo,
    this.fechaFin,
    this.sucursal,
  });

  DateTime fechaIni;
  String tipoComparativo;
  DateTime fechaFin;
  String sucursal;

  factory IndicadoresObtenerDto.fromJson(Map<String, dynamic> json) =>
      IndicadoresObtenerDto(
        fechaIni: DateTime.parse(json["fechaIni"]),
        tipoComparativo: json["TipoComparativo"],
        fechaFin: DateTime.parse(json["fechaFin"]),
        sucursal: json["Sucursal"],
      );

  Map<String, dynamic> toJson() => {
        "fechaIni":
            "${fechaIni.year.toString().padLeft(4, '0')}-${fechaIni.month.toString().padLeft(2, '0')}-${fechaIni.day.toString().padLeft(2, '0')}",
        "TipoComparativo": tipoComparativo,
        "fechaFin":
            "${fechaFin.year.toString().padLeft(4, '0')}-${fechaFin.month.toString().padLeft(2, '0')}-${fechaFin.day.toString().padLeft(2, '0')}",
        "Sucursal": sucursal,
      };
}
