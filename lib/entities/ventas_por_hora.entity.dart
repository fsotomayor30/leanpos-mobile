class VentasPorHora {
  VentasPorHora({
    this.hora,
    this.ventaPorHora,
    this.descuentosPorHora,
    this.anulacionesPorHora,
    this.clientesPorHora,
    this.horaVenta,
    this.cantidadTransacciones,
  });

  int hora;
  int ventaPorHora;
  int descuentosPorHora;
  int anulacionesPorHora;
  int clientesPorHora;
  dynamic horaVenta;
  String cantidadTransacciones;

  factory VentasPorHora.fromJson(Map<String, dynamic> json) => VentasPorHora(
        hora: json["hora"],
        ventaPorHora: json["ventaPorHora"],
        descuentosPorHora: json["descuentosPorHora"],
        anulacionesPorHora: json["anulacionesPorHora"],
        clientesPorHora: json["clientesPorHora"],
        horaVenta: json["horaVenta"],
        cantidadTransacciones: json["cantidadTransacciones"],
      );

  Map<String, dynamic> toJson() => {
        "hora": hora,
        "ventaPorHora": ventaPorHora,
        "descuentosPorHora": descuentosPorHora,
        "anulacionesPorHora": anulacionesPorHora,
        "clientesPorHora": clientesPorHora,
        "horaVenta": horaVenta,
        "cantidadTransacciones": cantidadTransacciones,
      };
}
