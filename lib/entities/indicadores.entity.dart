import 'package:leanpos/entities/rank.entity.dart';
import 'package:leanpos/entities/ranking_usuario.entity.dart';
import 'package:leanpos/entities/venta.entity.dart';
import 'package:leanpos/entities/ventas_por_ao.entity.dart';
import 'package:leanpos/entities/ventas_por_hora.entity.dart';
import 'package:leanpos/entities/ventas_por_sucursal.entity.dart';

class Indicadores {
  Indicadores({
    this.ventas,
    this.ventasTotales,
    this.ventasTotalesSinCs,
    this.descuentosCantidad,
    this.transaccionesCantidad,
    this.descuentoAvg,
    this.transaccionAvg,
    this.clienteAvg,
    this.clientesCantidad,
    this.clientesCantidadCom,
    this.descuentoAvgCom,
    this.transaccionAvgCom,
    this.clienteAvgCom,
    this.anulacionAvg,
    this.anulacionAvgCom,
    this.anulacionCantidad,
    this.anulacionCantidadCom,
    this.pmCantidadAnulacion,
    this.pmPromedioAnulacion,
    this.ventasPorHora,
    this.ventasPorSucursal,
    this.ventasPorAo,
    this.rankingProd,
    this.rankingCat,
    this.rankProdPrecio,
    this.rankCategoriaPrecio,
    this.pmTransaccion,
    this.pTransacCant,
    this.pmDescuento,
    this.pDescuentoCant,
    this.transacCantidadCom,
    this.descuentosCantidadCom,
    this.pmAvgCliente,
    this.pmCantidadCliente,
    this.rankingUsuarios,
    this.rankingUsuarioPrecio,
    this.rankingUsuariosA,
    this.rankingUsuarioPrecioA,
    this.totalProductoRanking,
    this.totalCategoriaRanking,
    this.usuariosVentasTotales,
    this.usuariosAnulacionesTotales,
  });

  List<Venta> ventas;
  int ventasTotales;
  int ventasTotalesSinCs;
  int descuentosCantidad;
  int transaccionesCantidad;
  int descuentoAvg;
  double transaccionAvg;
  int clienteAvg;
  int clientesCantidad;
  int clientesCantidadCom;
  int descuentoAvgCom;
  int transaccionAvgCom;
  int clienteAvgCom;
  int anulacionAvg;
  int anulacionAvgCom;
  int anulacionCantidad;
  int anulacionCantidadCom;
  double pmCantidadAnulacion;
  double pmPromedioAnulacion;
  List<VentasPorHora> ventasPorHora;
  List<VentasPorSucursal> ventasPorSucursal;
  List<VentasPorAo> ventasPorAo;
  List<Rank> rankingProd;
  List<Rank> rankingCat;
  List<Rank> rankProdPrecio;
  List<Rank> rankCategoriaPrecio;
  double pmTransaccion;
  int pTransacCant;
  double pmDescuento;
  int pDescuentoCant;
  int transacCantidadCom;
  int descuentosCantidadCom;
  double pmAvgCliente;
  double pmCantidadCliente;
  List<RankingUsuario> rankingUsuarios;
  List<RankingUsuario> rankingUsuarioPrecio;
  List<RankingUsuario> rankingUsuariosA;
  List<RankingUsuario> rankingUsuarioPrecioA;
  Map<String, double> totalProductoRanking;
  Map<String, double> totalCategoriaRanking;
  Map<String, double> usuariosVentasTotales;
  Map<String, double> usuariosAnulacionesTotales;

  factory Indicadores.fromJson(Map<String, dynamic> json) => Indicadores(
        ventas: List<Venta>.from(json["ventas"].map((x) => Venta.fromJson(x))),
        ventasTotales: json["ventasTotales"],
        ventasTotalesSinCs: json["ventasTotalesSinCS"],
        descuentosCantidad: json["descuentosCantidad"],
        transaccionesCantidad: json["transaccionesCantidad"],
        descuentoAvg: json["descuentoAvg"],
        transaccionAvg: json["transaccionAvg"].toDouble(),
        clienteAvg: json["clienteAvg"],
        clientesCantidad: json["clientesCantidad"],
        clientesCantidadCom: json["clientesCantidadCom"],
        descuentoAvgCom: json["descuentoAvgCom"],
        transaccionAvgCom: json["transaccionAvgCom"],
        clienteAvgCom: json["clienteAvgCom"],
        anulacionAvg: json["anulacionAvg"],
        anulacionAvgCom: json["anulacionAvgCom"],
        anulacionCantidad: json["anulacionCantidad"],
        anulacionCantidadCom: json["anulacionCantidadCom"],
        pmCantidadAnulacion: json["pmCantidadAnulacion"],
        pmPromedioAnulacion: json["pmPromedioAnulacion"],
        ventasPorHora: List<VentasPorHora>.from(
            json["ventasPorHora"].map((x) => VentasPorHora.fromJson(x))),
        ventasPorSucursal: List<VentasPorSucursal>.from(
            json["ventasPorSucursal"]
                .map((x) => VentasPorSucursal.fromJson(x))),
        ventasPorAo: List<VentasPorAo>.from(
            json["ventasPorAño"].map((x) => VentasPorAo.fromJson(x))),
        rankingProd:
            List<Rank>.from(json["rankingProd"].map((x) => Rank.fromJson(x))),
        rankingCat:
            List<Rank>.from(json["rankingCat"].map((x) => Rank.fromJson(x))),
        rankProdPrecio: List<Rank>.from(
            json["rankProdPrecio"].map((x) => Rank.fromJson(x))),
        rankCategoriaPrecio: List<Rank>.from(
            json["rankCategoriaPrecio"].map((x) => Rank.fromJson(x))),
        pmTransaccion: json["pmTransaccion"],
        pTransacCant: json["pTransacCant"],
        pmDescuento: json["pmDescuento"],
        pDescuentoCant: json["pDescuentoCant"],
        transacCantidadCom: json["transacCantidadCom"],
        descuentosCantidadCom: json["descuentosCantidadCom"],
        pmAvgCliente: json["pmAvgCliente"],
        pmCantidadCliente: json["pmCantidadCliente"],
        rankingUsuarios: List<RankingUsuario>.from(
            json["rankingUsuarios"].map((x) => RankingUsuario.fromJson(x))),
        rankingUsuarioPrecio: List<RankingUsuario>.from(
            json["rankingUsuarioPrecio"]
                .map((x) => RankingUsuario.fromJson(x))),
        rankingUsuariosA: List<RankingUsuario>.from(
            json["rankingUsuariosA"].map((x) => RankingUsuario.fromJson(x))),
        rankingUsuarioPrecioA: List<RankingUsuario>.from(
            json["rankingUsuarioPrecioA"]
                .map((x) => RankingUsuario.fromJson(x))),
        totalProductoRanking: Map.from(json["totalProductoRanking"]).map(
            (k, v) =>
                MapEntry<String, double>(k, v == null ? null : v.toDouble())),
        totalCategoriaRanking: Map.from(json["totalCategoriaRanking"]).map(
            (k, v) =>
                MapEntry<String, double>(k, v == null ? null : v.toDouble())),
        usuariosVentasTotales: Map.from(json["usuariosVentasTotales"])
            .map((k, v) => MapEntry<String, double>(k, v == null ? null : v)),
        usuariosAnulacionesTotales: Map.from(json["usuariosAnulacionesTotales"])
            .map((k, v) => MapEntry<String, double>(k, v == null ? null : v)),
      );

  Map<String, dynamic> toJson() => {
        "ventas": List<dynamic>.from(ventas.map((x) => x.toJson())),
        "ventasTotales": ventasTotales,
        "ventasTotalesSinCS": ventasTotalesSinCs,
        "descuentosCantidad": descuentosCantidad,
        "transaccionesCantidad": transaccionesCantidad,
        "descuentoAvg": descuentoAvg,
        "transaccionAvg": transaccionAvg,
        "clienteAvg": clienteAvg,
        "clientesCantidad": clientesCantidad,
        "clientesCantidadCom": clientesCantidadCom,
        "descuentoAvgCom": descuentoAvgCom,
        "transaccionAvgCom": transaccionAvgCom,
        "clienteAvgCom": clienteAvgCom,
        "anulacionAvg": anulacionAvg,
        "anulacionAvgCom": anulacionAvgCom,
        "anulacionCantidad": anulacionCantidad,
        "anulacionCantidadCom": anulacionCantidadCom,
        "pmCantidadAnulacion": pmCantidadAnulacion,
        "pmPromedioAnulacion": pmPromedioAnulacion,
        "ventasPorHora":
            List<dynamic>.from(ventasPorHora.map((x) => x.toJson())),
        "ventasPorSucursal":
            List<dynamic>.from(ventasPorSucursal.map((x) => x.toJson())),
        "ventasPorAño": List<dynamic>.from(ventasPorAo.map((x) => x.toJson())),
        "rankingProd": List<dynamic>.from(rankingProd.map((x) => x.toJson())),
        "rankingCat": List<dynamic>.from(rankingCat.map((x) => x.toJson())),
        "rankProdPrecio":
            List<dynamic>.from(rankProdPrecio.map((x) => x.toJson())),
        "rankCategoriaPrecio":
            List<dynamic>.from(rankCategoriaPrecio.map((x) => x.toJson())),
        "pmTransaccion": pmTransaccion,
        "pTransacCant": pTransacCant,
        "pmDescuento": pmDescuento,
        "pDescuentoCant": pDescuentoCant,
        "transacCantidadCom": transacCantidadCom,
        "descuentosCantidadCom": descuentosCantidadCom,
        "pmAvgCliente": pmAvgCliente,
        "pmCantidadCliente": pmCantidadCliente,
        "rankingUsuarios":
            List<dynamic>.from(rankingUsuarios.map((x) => x.toJson())),
        "rankingUsuarioPrecio":
            List<dynamic>.from(rankingUsuarioPrecio.map((x) => x.toJson())),
        "rankingUsuariosA":
            List<dynamic>.from(rankingUsuariosA.map((x) => x.toJson())),
        "rankingUsuarioPrecioA":
            List<dynamic>.from(rankingUsuarioPrecioA.map((x) => x.toJson())),
        "totalProductoRanking": Map.from(totalProductoRanking)
            .map((k, v) => MapEntry<String, dynamic>(k, v == null ? null : v)),
        "totalCategoriaRanking": Map.from(totalCategoriaRanking)
            .map((k, v) => MapEntry<String, dynamic>(k, v == null ? null : v)),
        "usuariosVentasTotales": Map.from(usuariosVentasTotales)
            .map((k, v) => MapEntry<String, dynamic>(k, v == null ? null : v)),
        "usuariosAnulacionesTotales": Map.from(usuariosAnulacionesTotales)
            .map((k, v) => MapEntry<String, dynamic>(k, v == null ? null : v)),
      };
}
