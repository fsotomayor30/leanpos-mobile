import 'package:leanpos/entities/sucursal.entity.dart';

class Usuarios {
  Usuarios({
    this.id,
    this.pin,
    this.nombre,
    this.apellido,
    this.tipoUsuarioId,
    this.tipoUsuario,
    this.cajaId,
    this.caja,
    this.sucursalId,
    this.sucursal,
    this.jwt,
    this.isSuperAdmin,
    this.isHabilitado,
  });

  int id;
  int pin;
  String nombre;
  String apellido;
  int tipoUsuarioId;
  dynamic tipoUsuario;
  int cajaId;
  dynamic caja;
  int sucursalId;
  Sucursal sucursal;
  dynamic jwt;
  bool isSuperAdmin;
  bool isHabilitado;

  factory Usuarios.fromJson(Map<String, dynamic> json) => Usuarios(
    id: json["id"],
    pin: json["pin"],
    nombre: json["nombre"],
    apellido: json["apellido"],
    tipoUsuarioId: json["tipoUsuarioID"],
    tipoUsuario: json["tipoUsuario"],
    cajaId: json["cajaID"],
    caja: json["caja"],
    sucursalId: json["sucursalID"],
    sucursal: json["sucursal"] == null ? null :  Sucursal.fromJson(json["sucursal"]),
    jwt: json["jwt"],
    isSuperAdmin: json["isSuperAdmin"],
    isHabilitado: json["isHabilitado"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pin": pin,
    "nombre": nombre,
    "apellido": apellido,
    "tipoUsuarioID": tipoUsuarioId,
    "tipoUsuario": tipoUsuario,
    "cajaID": cajaId,
    "caja": caja,
    "sucursalID": sucursalId,
    "sucursal": sucursal.toJson(),
    "jwt": jwt,
    "isSuperAdmin": isSuperAdmin,
    "isHabilitado": isHabilitado,
  };
}