import 'package:leanpos/entities/tb_modificadore.entity.dart';

class RelProdMod {
  RelProdMod({
    this.id,
    this.desrel,
    this.orden,
    this.sucursalId,
    this.sucursal,
    this.tbProductoId,
    this.tbModificadores,
    this.isObligatorio,
  });

  int id;
  String desrel;
  int orden;
  int sucursalId;
  dynamic sucursal;
  int tbProductoId;
  List<TbModificadore> tbModificadores;
  bool isObligatorio;

  factory RelProdMod.fromJson(Map<String, dynamic> json) => RelProdMod(
        id: json["id"],
        desrel: json["desrel"],
        orden: json["orden"],
        sucursalId: json["sucursalID"],
        sucursal: json["sucursal"],
        tbProductoId: json["tbProductoID"],
        tbModificadores: List<TbModificadore>.from(
            json["tbModificadores"].map((x) => TbModificadore.fromJson(x))),
        isObligatorio: json["isObligatorio"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "desrel": desrel,
        "orden": orden,
        "sucursalID": sucursalId,
        "sucursal": sucursal,
        "tbProductoID": tbProductoId,
        "tbModificadores":
            List<dynamic>.from(tbModificadores.map((x) => x.toJson())),
        "isObligatorio": isObligatorio,
      };
}
