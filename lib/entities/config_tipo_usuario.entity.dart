import 'package:leanpos/entities/sucursal.entity.dart';

class ConfigTipoUsuario {
  ConfigTipoUsuario({
    this.id,
    this.sucursalId,
    this.sucursal,
    this.urlInicio,
    this.tipoUsuarioId,
  });

  int id;
  int sucursalId;
  Sucursal sucursal;
  String urlInicio;
  int tipoUsuarioId;

  factory ConfigTipoUsuario.fromJson(Map<String, dynamic> json) => ConfigTipoUsuario(
    id: json["id"],
    sucursalId: json["sucursalID"],
    sucursal: json["sucursal"] == null ? null : Sucursal.fromJson(json["sucursal"]),
    urlInicio: json["urlInicio"],
    tipoUsuarioId: json["tipoUsuarioID"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "sucursalID": sucursalId,
    "sucursal": sucursal == null ? null : sucursal.toJson(),
    "urlInicio": urlInicio,
    "tipoUsuarioID": tipoUsuarioId,
  };
}