class ConfigPos {
  ConfigPos(
      {this.id,
      this.habilitada,
      this.horaAlmuerzo,
      this.horaComienzoDia,
      this.horaCena,
      this.isFacturacionActivada,
      this.maxIdle,
      this.isCodigoOperacion,
      this.urlRedireccionVentaM,
      this.balanzaApp,
      this.urlRedireccionParaLlevar,
      this.urlRedireccionBar,
      this.isKitchenDisplay,
      this.tiempoEspMax,
      this.isZonaImpresionHabilitado,
      this.isCrudModif,
      this.isCrudCuentaCasa,
      this.isCrudDescuentos,
      this.isCrudMesaAmbiente,
      this.isBarCode,
      this.isPromociones,
      this.isNivelSuperior,
      this.isReporteVentasPorProducto,
      this.isGruposFamilias,
      this.isPrecioPorZona,
      this.isParaLlevarHabilitado,
      this.isMesasHabilitado,
      this.isBarHabilitado,
      this.isDeliveryHabilitado,
      this.isStockHabilitado,
      this.isProdDisponibleHabilitado,
      this.openUrlRespuesta,
      this.isKdsPagadoHabilitado,
      this.isCuponDePagoActivo,
      this.isIntegrationActivo,
      this.isBotonCambiarPrecioActivo,
      this.cloudPrintActive});

  int id;
  int habilitada;
  String horaAlmuerzo;
  String horaComienzoDia;
  String horaCena;
  bool isFacturacionActivada;
  int maxIdle;
  bool isCodigoOperacion;
  String urlRedireccionVentaM;
  String balanzaApp;
  String urlRedireccionParaLlevar;
  String urlRedireccionBar;
  bool isKitchenDisplay;
  int tiempoEspMax;
  bool isZonaImpresionHabilitado;
  bool isCrudModif;
  bool isCrudCuentaCasa;
  bool isCrudDescuentos;
  bool isCrudMesaAmbiente;
  bool isBarCode;
  bool isPromociones;
  bool isNivelSuperior;
  bool isReporteVentasPorProducto;
  bool isGruposFamilias;
  bool isPrecioPorZona;
  bool isParaLlevarHabilitado;
  bool isMesasHabilitado;
  bool isBarHabilitado;
  bool isDeliveryHabilitado;
  bool isStockHabilitado;
  bool isProdDisponibleHabilitado;
  bool openUrlRespuesta;
  bool isKdsPagadoHabilitado;
  bool isCuponDePagoActivo;
  bool isIntegrationActivo;
  bool isBotonCambiarPrecioActivo;
  bool cloudPrintActive;

  factory ConfigPos.fromJson(Map<String, dynamic> json) => ConfigPos(
      id: json["id"],
      habilitada: json["habilitada"],
      horaAlmuerzo: json["horaAlmuerzo"],
      horaComienzoDia: json["horaComienzoDia"],
      horaCena: json["horaCena"],
      isFacturacionActivada: json["isFacturacionActivada"],
      maxIdle: json["maxIdle"],
      isCodigoOperacion: json["isCodigoOperacion"],
      urlRedireccionVentaM: json["urlRedireccionVentaM"],
      balanzaApp: json["balanzaApp"],
      urlRedireccionParaLlevar: json["urlRedireccionParaLlevar"],
      urlRedireccionBar: json["urlRedireccionBar"],
      isKitchenDisplay: json["isKitchenDisplay"],
      tiempoEspMax: json["tiempoEspMax"],
      isZonaImpresionHabilitado: json["isZonaImpresionHabilitado"],
      isCrudModif: json["isCrudModif"],
      isCrudCuentaCasa: json["isCrudCuentaCasa"],
      isCrudDescuentos: json["isCrudDescuentos"],
      isCrudMesaAmbiente: json["isCrudMesaAmbiente"],
      isBarCode: json["isBarCode"],
      isPromociones: json["isPromociones"],
      isNivelSuperior: json["isNivelSuperior"],
      isReporteVentasPorProducto: json["isReporteVentasPorProducto"],
      isGruposFamilias: json["isGruposFamilias"],
      isPrecioPorZona: json["isPrecioPorZona"],
      isParaLlevarHabilitado: json["isParaLlevarHabilitado"],
      isMesasHabilitado: json["isMesasHabilitado"],
      isBarHabilitado: json["isBarHabilitado"],
      isDeliveryHabilitado: json["isDeliveryHabilitado"],
      isStockHabilitado: json["isStockHabilitado"],
      isProdDisponibleHabilitado: json["isProdDisponibleHabilitado"],
      openUrlRespuesta: json["openUrlRespuesta"],
      isKdsPagadoHabilitado: json["isKdsPagadoHabilitado"],
      isCuponDePagoActivo: json["isCuponDePagoActivo"],
      isIntegrationActivo: json["isIntegrationActivo"],
      isBotonCambiarPrecioActivo: json["isBotonCambiarPrecioActivo"],
      cloudPrintActive: json["cloudPrintActive"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "habilitada": habilitada,
        "horaAlmuerzo": horaAlmuerzo,
        "horaComienzoDia": horaComienzoDia,
        "horaCena": horaCena,
        "isFacturacionActivada": isFacturacionActivada,
        "maxIdle": maxIdle,
        "isCodigoOperacion": isCodigoOperacion,
        "urlRedireccionVentaM": urlRedireccionVentaM,
        "balanzaApp": balanzaApp,
        "urlRedireccionParaLlevar": urlRedireccionParaLlevar,
        "urlRedireccionBar": urlRedireccionBar,
        "isKitchenDisplay": isKitchenDisplay,
        "tiempoEspMax": tiempoEspMax,
        "isZonaImpresionHabilitado": isZonaImpresionHabilitado,
        "isCrudModif": isCrudModif,
        "isCrudCuentaCasa": isCrudCuentaCasa,
        "isCrudDescuentos": isCrudDescuentos,
        "isCrudMesaAmbiente": isCrudMesaAmbiente,
        "isBarCode": isBarCode,
        "isPromociones": isPromociones,
        "isNivelSuperior": isNivelSuperior,
        "isReporteVentasPorProducto": isReporteVentasPorProducto,
        "isGruposFamilias": isGruposFamilias,
        "isPrecioPorZona": isPrecioPorZona,
        "isParaLlevarHabilitado": isParaLlevarHabilitado,
        "isMesasHabilitado": isMesasHabilitado,
        "isBarHabilitado": isBarHabilitado,
        "isDeliveryHabilitado": isDeliveryHabilitado,
        "isStockHabilitado": isStockHabilitado,
        "isProdDisponibleHabilitado": isProdDisponibleHabilitado,
        "openUrlRespuesta": openUrlRespuesta,
        "isKdsPagadoHabilitado": isKdsPagadoHabilitado,
        "isCuponDePagoActivo": isCuponDePagoActivo,
        "isIntegrationActivo": isIntegrationActivo,
        "isBotonCambiarPrecioActivo": isBotonCambiarPrecioActivo,
        "cloudPrintActive": cloudPrintActive
      };

  @override
  String toString() {
    return 'ConfigPos{id: $id, habilitada: $habilitada, horaAlmuerzo: $horaAlmuerzo, horaComienzoDia: $horaComienzoDia, horaCena: $horaCena, isFacturacionActivada: $isFacturacionActivada, maxIdle: $maxIdle, isCodigoOperacion: $isCodigoOperacion, urlRedireccionVentaM: $urlRedireccionVentaM, balanzaApp: $balanzaApp, urlRedireccionParaLlevar: $urlRedireccionParaLlevar, urlRedireccionBar: $urlRedireccionBar, isKitchenDisplay: $isKitchenDisplay, tiempoEspMax: $tiempoEspMax, isZonaImpresionHabilitado: $isZonaImpresionHabilitado, isCrudModif: $isCrudModif, isCrudCuentaCasa: $isCrudCuentaCasa, isCrudDescuentos: $isCrudDescuentos, isCrudMesaAmbiente: $isCrudMesaAmbiente, isBarCode: $isBarCode, isPromociones: $isPromociones, isNivelSuperior: $isNivelSuperior, isReporteVentasPorProducto: $isReporteVentasPorProducto, isGruposFamilias: $isGruposFamilias, isPrecioPorZona: $isPrecioPorZona, isParaLlevarHabilitado: $isParaLlevarHabilitado, isMesasHabilitado: $isMesasHabilitado, isBarHabilitado: $isBarHabilitado, isDeliveryHabilitado: $isDeliveryHabilitado, isStockHabilitado: $isStockHabilitado, isProdDisponibleHabilitado: $isProdDisponibleHabilitado, openUrlRespuesta: $openUrlRespuesta, isKdsPagadoHabilitado: $isKdsPagadoHabilitado, isCuponDePagoActivo: $isCuponDePagoActivo, isIntegrationActivo: $isIntegrationActivo, isBotonCambiarPrecioActivo: $isBotonCambiarPrecioActivo}';
  }
}
