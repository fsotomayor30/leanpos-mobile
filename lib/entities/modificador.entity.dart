class Modificador {
  Modificador({
    this.id,
    this.desmod,
    this.habilita,
    this.isProducto,
    this.fecIngreso,
    this.usuIngreso,
    this.fecModifica,
    this.usuModifica,
    this.sucursalId,
    this.sucursal,
    this.relProdModId,
    this.relProdMod,
    this.tbProductoId,
    this.tbProducto,
    this.isCombo,
    this.productoAsociadoId,
  });

  int id;
  String desmod;
  int habilita;
  bool isProducto;
  DateTime fecIngreso;
  String usuIngreso;
  DateTime fecModifica;
  dynamic usuModifica;
  int sucursalId;
  dynamic sucursal;
  int relProdModId;
  dynamic relProdMod;
  int tbProductoId;
  dynamic tbProducto;
  bool isCombo;
  dynamic productoAsociadoId;

  factory Modificador.fromJson(Map<String, dynamic> json) => Modificador(
        id: json["id"],
        desmod: json["desmod"],
        habilita: json["habilita"],
        isProducto: json["isProducto"],
        fecIngreso: DateTime.parse(json["fecIngreso"]),
        usuIngreso: json["usuIngreso"],
        fecModifica: DateTime.parse(json["fecModifica"]),
        usuModifica: json["usuModifica"],
        sucursalId: json["sucursalID"],
        sucursal: json["sucursal"],
        relProdModId:
            json["relProdModID"] == null ? null : json["relProdModID"],
        relProdMod: json["relProdMod"],
        tbProductoId:
            json["tbProductoID"] == null ? null : json["tbProductoID"],
        tbProducto: json["tbProducto"],
        isCombo: json["isCombo"],
        productoAsociadoId: json["productoAsociadoID"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "desmod": desmod,
        "habilita": habilita,
        "isProducto": isProducto,
        "fecIngreso": fecIngreso.toIso8601String(),
        "usuIngreso": usuIngreso,
        "fecModifica": fecModifica.toIso8601String(),
        "usuModifica": usuModifica,
        "sucursalID": sucursalId,
        "sucursal": sucursal,
        "relProdModID": relProdModId == null ? null : relProdModId,
        "relProdMod": relProdMod,
        "tbProductoID": tbProductoId == null ? null : tbProductoId,
        "tbProducto": tbProducto,
        "isCombo": isCombo,
        "productoAsociadoID": productoAsociadoId,
      };
}
