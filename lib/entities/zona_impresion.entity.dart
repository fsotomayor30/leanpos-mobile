class ZonaImpresion {
  ZonaImpresion({
    this.id,
    this.descripcion,
    this.impresoraIp,
  });

  int id;
  String descripcion;
  String impresoraIp;

  factory ZonaImpresion.fromJson(Map<String, dynamic> json) => ZonaImpresion(
    id: json["id"],
    descripcion: json["descripcion"],
    impresoraIp: json["impresoraIP"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "descripcion": descripcion,
    "impresoraIP": impresoraIp,
  };
}