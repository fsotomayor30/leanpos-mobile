// To parse this JSON data, do
//
//     final mesa = mesaFromJson(jsonString);

import 'dart:convert';

import 'package:leanpos/entities/sucursal.entity.dart';

Mesa mesaFromJson(String str) => Mesa.fromJson(json.decode(str));

String mesaToJson(Mesa data) => json.encode(data.toJson());

class Mesa {
  Mesa({
    this.id,
    this.descipcion,
    this.ambienteId,
    this.ambiente,
    this.capacidad,
    this.bitacorasMesa,
    this.sucursalId,
    this.sucursal,
    this.isHabilitado,
    this.fechaCreacion,
    this.usuModifica,
    this.fechaModifica,
    this.ordenId,
    this.isOcupada,
    this.isImpresa,
    this.isPagada,
    this.usuario,
    this.timeOcupada,
    this.numOrdenesAbiertas,
  });

  int id;
  String descipcion;
  int ambienteId;
  dynamic ambiente;
  int capacidad;
  dynamic bitacorasMesa;
  int sucursalId;
  Sucursal sucursal;
  bool isHabilitado;
  DateTime fechaCreacion;
  dynamic usuModifica;
  DateTime fechaModifica;
  int ordenId;
  bool isOcupada;
  bool isImpresa;
  bool isPagada;
  String usuario;
  String timeOcupada;
  int numOrdenesAbiertas;

  factory Mesa.fromJson(Map<String, dynamic> json) => Mesa(
    id: json["id"],
    ordenId: json["ordenID"],
    capacidad: json["capacidad"],
    ambienteId: json["ambienteID"],
    ambiente: json["ambiente"],
    descipcion: json["descipcion"],
    isOcupada: json["isOcupada"],
    isImpresa: json["isImpresa"],
    isPagada: json["isPagada"],
    usuario: json["usuario"],
    timeOcupada: json["timeOcupada"],
    numOrdenesAbiertas: json["numOrdenesAbiertas"],
    sucursalId: json["sucursalID"],
    bitacorasMesa: json["bitacorasMesa"],
    sucursal: json["sucursal"] == null ? null : Sucursal.fromJson(json["sucursal"]),
    isHabilitado: json["isHabilitado"],
    fechaCreacion: json["fechaCreacion"] == null ? null : DateTime.parse(json["fechaCreacion"]),
    usuModifica: json["usuModifica"],
    fechaModifica: json["fechaModifica"] == null ? null : DateTime.parse(json["fechaModifica"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "ordenID": ordenId,
    "capacidad": capacidad,
    "ambienteID": ambienteId,
    "ambiente": ambiente,
    "descipcion": descipcion,
    "isOcupada": isOcupada,
    "isImpresa": isImpresa,
    "isPagada": isPagada,
    "usuario": usuario,
    "timeOcupada": timeOcupada,
    "numOrdenesAbiertas": numOrdenesAbiertas,
    "sucursalID": sucursalId,
    "bitacorasMesa": bitacorasMesa,
    "sucursal": sucursal.toJson(),
    "isHabilitado": isHabilitado,
    "fechaCreacion": fechaCreacion.toIso8601String(),
    "usuModifica": usuModifica,
    "fechaModifica": fechaModifica.toIso8601String()
  };
}
