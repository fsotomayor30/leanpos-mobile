import 'package:leanpos/entities/sucursal.entity.dart';

class Caja {
  Caja({
    this.id,
    this.descripcion,
    this.isHabilitada,
    this.fecIngreso,
    this.usuIngreso,
    this.fecModifica,
    this.usuModifica,
    this.sucursalId,
    this.sucursal,
    this.gavetas,
    this.salidasDineros,
  });

  int id;
  String descripcion;
  bool isHabilitada;
  DateTime fecIngreso;
  String usuIngreso;
  DateTime fecModifica;
  String usuModifica;
  int sucursalId;
  Sucursal sucursal;
  dynamic gavetas;
  dynamic salidasDineros;

  factory Caja.fromJson(Map<String, dynamic> json) => Caja(
    id: json["id"],
    descripcion: json["descripcion"],
    isHabilitada: json["isHabilitada"],
    fecIngreso: DateTime.parse(json["fecIngreso"]),
    usuIngreso: json["usuIngreso"],
    fecModifica: DateTime.parse(json["fecModifica"]),
    usuModifica: json["usuModifica"],
    sucursalId: json["sucursalID"],
    sucursal: Sucursal.fromJson(json["sucursal"]),
    gavetas: json["gavetas"],
    salidasDineros: json["salidasDineros"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "descripcion": descripcion,
    "isHabilitada": isHabilitada,
    "fecIngreso": fecIngreso.toIso8601String(),
    "usuIngreso": usuIngreso,
    "fecModifica": fecModifica.toIso8601String(),
    "usuModifica": usuModifica,
    "sucursalID": sucursalId,
    "sucursal": sucursal.toJson(),
    "gavetas": gavetas,
    "salidasDineros": salidasDineros,
  };
}
