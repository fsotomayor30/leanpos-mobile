import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/entities/sucursal.entity.dart';

class Tbgrupo {
  Tbgrupo({
    this.id,
    this.desgrup,
    this.dispMesas,
    this.dispLlevar,
    this.ispDelivery,
    this.dispPedido,
    this.estgrup,
    this.img,
    this.color,
    this.orden,
    this.vtacad,
    this.codmod,
    this.modforz,
    this.sucursalId,
    this.sucursal,
    this.isHabilitado,
    this.tbproductos,
  });

  int id;
  String desgrup;
  int dispMesas;
  int dispLlevar;
  int ispDelivery;
  int dispPedido;
  int estgrup;
  dynamic img;
  dynamic color;
  int orden;
  dynamic vtacad;
  dynamic codmod;
  int modforz;
  int sucursalId;
  Sucursal sucursal;
  bool isHabilitado;
  List<Producto> tbproductos;

  factory Tbgrupo.fromJson(Map<String, dynamic> json) => Tbgrupo(
    id: json["id"],
    desgrup: json["desgrup"],
    dispMesas: json["dispMesas"],
    dispLlevar: json["dispLlevar"],
    ispDelivery: json["ispDelivery"],
    dispPedido: json["dispPedido"],
    estgrup: json["estgrup"],
    img: json["img"],
    color: json["color"],
    orden: json["orden"],
    vtacad: json["vtacad"],
    codmod: json["codmod"],
    modforz: json["modforz"],
    sucursalId: json["sucursalID"],
    sucursal: Sucursal.fromJson(json["sucursal"]),
    isHabilitado: json["isHabilitado"],
    tbproductos: List<Producto>.from(json["tbproductos"].map((x) => Producto.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "desgrup": desgrup,
    "dispMesas": dispMesas,
    "dispLlevar": dispLlevar,
    "ispDelivery": ispDelivery,
    "dispPedido": dispPedido,
    "estgrup": estgrup,
    "img": img,
    "color": color,
    "orden": orden,
    "vtacad": vtacad,
    "codmod": codmod,
    "modforz": modforz,
    "sucursalID": sucursalId,
    "sucursal": sucursal.toJson(),
    "isHabilitado": isHabilitado,
    "tbproductos": List<dynamic>.from(tbproductos.map((x) => x.toJson())),
  };
}