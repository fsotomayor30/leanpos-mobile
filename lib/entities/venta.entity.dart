class Venta {
  Venta({
    this.cantidad,
    this.venta,
    this.tipoVenta,
  });

  int cantidad;
  int venta;
  String tipoVenta;

  factory Venta.fromJson(Map<String, dynamic> json) => Venta(
        cantidad: json["cantidad"],
        venta: json["venta"],
        tipoVenta: json["tipoVenta"],
      );

  Map<String, dynamic> toJson() => {
        "cantidad": cantidad,
        "venta": venta,
        "tipoVenta": tipoVenta,
      };
}
