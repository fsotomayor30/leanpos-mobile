class TiposVenta {
  TiposVenta({
    this.id,
    this.descripcion,
  });

  int id;
  String descripcion;

  factory TiposVenta.fromJson(Map<String, dynamic> json) => TiposVenta(
    id: json["id"],
    descripcion: json["descripcion"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "descripcion": descripcion,
  };
}