class VentasPorSucursal {
  VentasPorSucursal({
    this.sucursalId,
    this.ventaPorSuc,
    this.descuentosSuc,
    this.anulacionesSuc,
    this.clientesSuc,
    this.sucursal,
    this.cantidadTransacciones,
  });

  dynamic sucursalId;
  int ventaPorSuc;
  int descuentosSuc;
  int anulacionesSuc;
  int clientesSuc;
  String sucursal;
  String cantidadTransacciones;

  factory VentasPorSucursal.fromJson(Map<String, dynamic> json) =>
      VentasPorSucursal(
        sucursalId: json["sucursalID"],
        ventaPorSuc: json["ventaPorSuc"],
        descuentosSuc: json["descuentosSuc"],
        anulacionesSuc: json["anulacionesSuc"],
        clientesSuc: json["clientesSuc"],
        sucursal: json["sucursal"],
        cantidadTransacciones: json["cantidadTransacciones"],
      );

  Map<String, dynamic> toJson() => {
        "sucursalID": sucursalId,
        "ventaPorSuc": ventaPorSuc,
        "descuentosSuc": descuentosSuc,
        "anulacionesSuc": anulacionesSuc,
        "clientesSuc": clientesSuc,
        "sucursal": sucursal,
        "cantidadTransacciones": cantidadTransacciones,
      };
}
