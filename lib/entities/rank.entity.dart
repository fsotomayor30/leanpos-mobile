class Rank {
  Rank({
    this.productName,
    this.codigoProducto,
    this.categoriaName,
    this.codigoCategoria,
    this.cantidad,
    this.cantidadProd,
    this.precio,
    this.porcentajeCantidad,
    this.porcentajeCantidadD,
    this.porcentajePrecio,
    this.valorTotal,
    this.valorProd,
    this.precioNeto,
    this.precioBruto,
  });

  String productName;
  String codigoProducto;
  String categoriaName;
  String codigoCategoria;
  double cantidad;
  double cantidadProd;
  dynamic precio;
  double porcentajeCantidad;
  double porcentajeCantidadD;
  double porcentajePrecio;
  double valorTotal;
  double valorProd;
  dynamic precioNeto;
  dynamic precioBruto;

  factory Rank.fromJson(Map<String, dynamic> json) => Rank(
        productName: json["productName"] == null ? null : json["productName"],
        codigoProducto:
            json["codigoProducto"] == null ? null : json["codigoProducto"],
        categoriaName:
            json["categoriaName"] == null ? null : json["categoriaName"],
        codigoCategoria:
            json["codigoCategoria"] == null ? null : json["codigoCategoria"],
        cantidad: json["cantidad"],
        cantidadProd: json["cantidadProd"],
        precio: json["precio"],
        porcentajeCantidad: json["porcentajeCantidad"].toDouble(),
        porcentajeCantidadD: json["porcentajeCantidadD"],
        porcentajePrecio: json["porcentajePrecio"].toDouble(),
        valorTotal: json["valorTotal"],
        valorProd: json["valorProd"],
        precioNeto: json["precioNeto"],
        precioBruto: json["precioBruto"],
      );

  Map<String, dynamic> toJson() => {
        "productName": productName == null ? null : productName,
        "codigoProducto": codigoProducto == null ? null : codigoProducto,
        "categoriaName": categoriaName == null ? null : categoriaName,
        "codigoCategoria": codigoCategoria == null ? null : codigoCategoria,
        "cantidad": cantidad,
        "cantidadProd": cantidadProd,
        "precio": precio,
        "porcentajeCantidad": porcentajeCantidad,
        "porcentajeCantidadD": porcentajeCantidadD,
        "porcentajePrecio": porcentajePrecio,
        "valorTotal": valorTotal,
        "valorProd": valorProd,
        "precioNeto": precioNeto,
        "precioBruto": precioBruto,
      };
}
