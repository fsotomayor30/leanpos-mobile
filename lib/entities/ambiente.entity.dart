import 'dart:convert';

Ambiente ambienteFromJson(String str) => Ambiente.fromJson(json.decode(str));

String ambienteToJson(Ambiente data) => json.encode(data.toJson());

class Ambiente {
  Ambiente({
    this.id,
    this.descipcion,
    this.sucursalId,
    this.sucursal,
    this.isHabilitado,
    this.orden,
    this.fechaCreacion,
    this.usuModifica,
    this.fechaModifica,
  });

  int id;
  String descipcion;
  int sucursalId;
  dynamic sucursal;
  bool isHabilitado;
  int orden;
  DateTime fechaCreacion;
  String usuModifica;
  DateTime fechaModifica;

  factory Ambiente.fromJson(Map<String, dynamic> json) => Ambiente(
    id: json["id"],
    descipcion: json["descipcion"],
    sucursalId: json["sucursalID"],
    sucursal: json["sucursal"],
    isHabilitado: json["isHabilitado"],
    orden: json["orden"],
    fechaCreacion: DateTime.parse(json["fechaCreacion"]),
    usuModifica: json["usuModifica"],
    fechaModifica: DateTime.parse(json["fechaModifica"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "descipcion": descipcion,
    "sucursalID": sucursalId,
    "sucursal": sucursal,
    "isHabilitado": isHabilitado,
    "orden": orden,
    "fechaCreacion": fechaCreacion.toIso8601String(),
    "usuModifica": usuModifica,
    "fechaModifica": fechaModifica.toIso8601String(),
  };

  @override
  String toString() {
    return 'Ambiente{id: $id, descipcion: $descipcion, sucursalId: $sucursalId, sucursal: $sucursal, isHabilitado: $isHabilitado, orden: $orden, fechaCreacion: $fechaCreacion, usuModifica: $usuModifica, fechaModifica: $fechaModifica}';
  }
}
