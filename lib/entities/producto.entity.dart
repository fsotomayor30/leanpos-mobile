// To parse this JSON data, do
//
//     final producto = productoFromJson(jsonString);

import 'dart:convert';

import 'package:leanpos/entities/rel_prod_mod.entity.dart';
import 'package:leanpos/entities/sucursal.entity.dart';
import 'package:leanpos/entities/tb_grupo.entity.dart';

Producto productoFromJson(String str) => Producto.fromJson(json.decode(str));

String productoToJson(Producto data) => json.encode(data.toJson());

class Producto {
  Producto(
      {this.id,
      this.desprod,
      this.prodsimil,
      this.precpred,
      this.precmesa,
      this.precbar,
      this.precllevar,
      this.precpedido,
      this.precdeliv,
      this.permdesct,
      this.porcdesct,
      this.tipomodif,
      this.nivlsuper,
      this.nivlsupgr1,
      this.codimpres,
      this.codbarra,
      this.estprod,
      this.img,
      this.vtacad,
      this.codmod,
      this.glosa,
      this.prodrec,
      this.isCombo,
      this.tbgrupoId,
      this.tbgrupo,
      this.relProdMods,
      this.isPesable,
      this.precioKg,
      this.sucursalId,
      this.sucursal,
      this.plu,
      this.disponible,
      this.impuesto,
      this.impuestoCant,
      this.zonaImpresion,
      this.zonaImpresionId,
      this.isHabilitado,
      this.isNivelSuperior,
      this.productosInferiores,
      this.tbProductoId,
      this.grupoId,
      this.grupo,
      this.precioKgMesa,
      this.precioKgBar,
      this.precioKgDelivery,
      this.descripcionWeb,
      this.cantidad,
      this.modificadores,
      this.isComanda,
      this.descuentoPorc,
      this.descuentoPorcentaje,
      this.stock,
      this.isGuardado,
      this.codgrup,
      this.precioFinal});

  int id;
  String desprod;
  int prodsimil;
  int precpred;
  int precmesa;
  int precbar;
  int precllevar;
  int precpedido;
  int precdeliv;
  int permdesct;
  int porcdesct;
  int tipomodif;
  int nivlsuper;
  int nivlsupgr1;
  int codimpres;
  dynamic codbarra;
  int estprod;
  dynamic img;
  int vtacad;
  int codmod;
  dynamic glosa;
  int prodrec;
  bool isCombo;
  int tbgrupoId;
  Tbgrupo tbgrupo;
  List<RelProdMod> relProdMods;
  bool isPesable;
  int precioKg;
  int sucursalId;
  Sucursal sucursal;
  String plu;
  bool disponible;
  bool impuesto;
  String impuestoCant;
  dynamic zonaImpresion;
  int zonaImpresionId;
  bool isHabilitado;
  bool isNivelSuperior;
  List<dynamic> productosInferiores;
  dynamic tbProductoId;
  int grupoId;
  dynamic grupo;
  int precioKgMesa;
  int precioKgBar;
  int precioKgDelivery;
  String descripcionWeb;
  int cantidad;
  List<String> modificadores;
  bool isComanda;
  int descuentoPorc;
  int descuentoPorcentaje;
  int stock;
  bool isGuardado;
  int codgrup;
  int precioFinal;

  factory Producto.fromJson(Map<String, dynamic> json) => Producto(
        id: json["id"],
        desprod: json["desprod"],
        prodsimil: json["prodsimil"],
        precpred: json["precpred"],
        precmesa: json["precmesa"],
        precbar: json["precbar"],
        precllevar: json["precllevar"],
        precpedido: json["precpedido"],
        precdeliv: json["precdeliv"],
        permdesct: json["permdesct"],
        porcdesct: json["porcdesct"],
        tipomodif: json["tipomodif"],
        nivlsuper: json["nivlsuper"],
        nivlsupgr1: json["nivlsupgr1"],
        codimpres: json["codimpres"],
        codbarra: json["codbarra"],
        estprod: json["estprod"],
        img: json["img"],
        vtacad: json["vtacad"],
        codmod: json["codmod"],
        glosa: json["glosa"],
        prodrec: json["prodrec"],
        isCombo: json["isCombo"],
        tbgrupoId: json["tbgrupoID"],
        tbgrupo:
            json["tbgrupo"] == null ? null : Tbgrupo.fromJson(json["tbgrupo"]),
        relProdMods: json["relProdMods"] == null
            ? null
            : List<RelProdMod>.from(
                json["relProdMods"].map((x) => RelProdMod.fromJson(x))),
        isPesable: json["isPesable"],
        precioKg: json["precioKg"],
        sucursalId: json["sucursalID"],
        sucursal: json["sucursal"] == null
            ? null
            : Sucursal.fromJson(json["sucursal"]),
        plu: json["plu"],
        disponible: json["disponible"],
        impuesto: json["impuesto"],
        impuestoCant:
            json["impuestoCant"] == null ? null : json["impuestoCant"],
        zonaImpresion: json["zonaImpresion"],
        zonaImpresionId: json["zonaImpresionID"],
        isHabilitado: json["isHabilitado"],
        isNivelSuperior: json["isNivelSuperior"],
        productosInferiores: json["productosInferiores"] == null
            ? null
            : List<dynamic>.from(json["productosInferiores"].map((x) => x)),
        tbProductoId: json["tbProductoID"],
        grupoId: json["grupoID"],
        grupo: json["grupo"],
        precioKgMesa: json["precioKgMesa"],
        precioKgBar: json["precioKgBar"],
        precioKgDelivery: json["precioKgDelivery"],
        descripcionWeb:
            json["descripcionWeb"] == null ? null : json["descripcionWeb"],
        cantidad: json["cantidad"],
        modificadores: json["modificadores"] == null
            ? []
            : List<String>.from(json["modificadores"].map((x) => x)),
        isComanda: json["isComanda"],
        descuentoPorc: json["descuentoPorc"],
        descuentoPorcentaje: json["descuentoPorcentaje"],
        stock: json["stock"],
        codgrup: json["codgrup"] == null ? null : json["codgrup"],
        precioFinal: json["precioFinal"] == null ? null : json["precioFinal"],
        isGuardado: json["isGuardado"] == null ? null : json["isGuardado"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "desprod": desprod,
        "prodsimil": prodsimil,
        "precpred": precpred,
        "precmesa": precmesa,
        "precbar": precbar,
        "precllevar": precllevar,
        "precpedido": precpedido,
        "precdeliv": precdeliv,
        "permdesct": permdesct,
        "porcdesct": porcdesct,
        "tipomodif": tipomodif,
        "nivlsuper": nivlsuper,
        "nivlsupgr1": nivlsupgr1,
        "codimpres": codimpres,
        "codbarra": codbarra,
        "estprod": estprod,
        "img": img,
        "vtacad": vtacad,
        "codmod": codmod,
        "glosa": glosa,
        "prodrec": prodrec,
        "isCombo": isCombo,
        "tbgrupoID": tbgrupoId,
        "tbgrupo": tbgrupo == null ? null : tbgrupo.toJson(),
        "relProdMods": relProdMods,
        "isPesable": isPesable,
        "precioKg": precioKg,
        "sucursalID": sucursalId,
        "sucursal": sucursal == null ? null : sucursal.toJson(),
        "plu": plu,
        "disponible": disponible,
        "impuesto": impuesto,
        "impuestoCant": impuestoCant == null ? null : impuestoCant,
        "zonaImpresion": zonaImpresion,
        "zonaImpresionID": zonaImpresionId,
        "isHabilitado": isHabilitado,
        "isNivelSuperior": isNivelSuperior,
        "productosInferiores": productosInferiores == null
            ? null
            : List<dynamic>.from(productosInferiores.map((x) => x)),
        "tbProductoID": tbProductoId,
        "grupoID": grupoId,
        "grupo": grupo,
        "precioKgMesa": precioKgMesa,
        "precioKgBar": precioKgBar,
        "precioKgDelivery": precioKgDelivery,
        "descripcionWeb": descripcionWeb == null ? null : descripcionWeb,
        "cantidad": cantidad,
        "modificadores": modificadores,
        "isComanda": isComanda,
        "descuentoPorc": descuentoPorc,
        "descuentoPorcentaje": descuentoPorcentaje,
        "stock": stock,
        "codgrup": codgrup == null ? null : codgrup,
        "precioFinal": precioFinal == null ? null : precioFinal,
        "isGuardado": isGuardado == null ? null : isGuardado,
      };
}
