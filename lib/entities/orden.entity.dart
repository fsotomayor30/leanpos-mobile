import 'dart:convert';

import 'package:leanpos/entities/mesa.entity.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/entities/productos_orden.entity.dart';
import 'package:leanpos/entities/sucursal.entity.dart';
import 'package:leanpos/entities/tipos_de_venta.entity.dart';
import 'package:leanpos/entities/usuarios.entity.dart';

Orden ordenFromJson(String str) => Orden.fromJson(json.decode(str));

String ordenToJson(Orden data) => json.encode(data.toJson());

class Orden {
  Orden({
    this.id,
    this.fecha,
    this.hora,
    this.usuariosId,
    this.usuarios,
    this.pagado,
    this.propinaCant,
    this.propinaDif,
    this.descEfectivo,
    this.dctoOrden,
    this.dctoArt,
    this.dctoEfec,
    this.total,
    this.subtotal,
    this.urlDocFacturacion,
    this.numFolio,
    this.idBsale,
    this.tedSii,
    this.tipoDocumentoSii,
    this.horaTermino,
    this.fechaTermino,
    this.cajaId,
    this.caja,
    this.tiposVentaId,
    this.tiposVenta,
    this.sucursalId,
    this.sucursal,
    this.detalleDescuentoId,
    this.detalleDescuento,
    this.cierreCajaId,
    this.cierreCaja,
    this.gavetaId,
    this.gaveta,
    this.clienteId,
    this.cliente,
    this.infoDeliveryId,
    this.infoDelivery,
    this.mediosPorOrdens,
    this.productosOrdens,
    this.isAnulada,
    this.motivoAnulacion,
    this.carroProductos,
    this.carroProductosDos,
    this.isPendiente,
    this.mesaId,
    this.mesa,
    this.propinaPorc,
    this.numeroDeClientes,
    this.nombreCliente,
    this.isImpresa,
    this.numOrden,
    this.isListo,
    this.fechaOrdenLista,
    this.jsonFacturacionSii,
    this.jsonRespBsale,
    this.fechaFacturacion,
    this.pagadoDelivery,
    this.listaMediosPago,
    this.minutosDesdeApertura,
    this.idPedidoDelivery
  });

  int id;
  DateTime fecha;
  String hora;
  int usuariosId;
  Usuarios usuarios;
  bool pagado;
  int propinaCant;
  int propinaDif;
  int descEfectivo;
  int dctoOrden;
  int dctoArt;
  int dctoEfec;
  int total;
  int subtotal;
  dynamic urlDocFacturacion;
  dynamic numFolio;
  dynamic idBsale;
  dynamic tedSii;
  dynamic tipoDocumentoSii;
  dynamic horaTermino;
  dynamic fechaTermino;
  int cajaId;
  dynamic caja;
  int tiposVentaId;
  TiposVenta tiposVenta;
  int sucursalId;
  Sucursal sucursal;
  dynamic detalleDescuentoId;
  dynamic detalleDescuento;
  dynamic cierreCajaId;
  dynamic cierreCaja;
  int gavetaId;
  dynamic gaveta;
  dynamic clienteId;
  dynamic cliente;
  dynamic infoDeliveryId;
  dynamic infoDelivery;
  List<dynamic> mediosPorOrdens;
  List<ProductosOrden> productosOrdens;
  bool isAnulada;
  dynamic motivoAnulacion;
  List<Producto> carroProductos;
  dynamic carroProductosDos;
  bool isPendiente;
  int mesaId;
  Mesa mesa;
  int propinaPorc;
  int numeroDeClientes;
  dynamic nombreCliente;
  bool isImpresa;
  int numOrden;
  bool isListo;
  DateTime fechaOrdenLista;
  dynamic jsonFacturacionSii;
  dynamic jsonRespBsale;
  DateTime fechaFacturacion;
  dynamic pagadoDelivery;
  dynamic idPedidoDelivery;
  dynamic listaMediosPago;
  int minutosDesdeApertura;

  factory Orden.fromJson(Map<String, dynamic> json) => Orden(
    id: json["id"],
    fecha: DateTime.parse(json["fecha"]),
    hora: json["hora"],
    usuariosId: json["usuariosID"],
    usuarios: Usuarios.fromJson(json["usuarios"]),
    pagado: json["pagado"],
    propinaCant: json["propinaCant"],
    propinaDif: json["propinaDif"],
    descEfectivo: json["descEfectivo"],
    dctoOrden: json["dctoOrden"],
    dctoArt: json["dctoArt"],
    dctoEfec: json["dctoEfec"],
    total: json["total"],
    subtotal: json["subtotal"],
    urlDocFacturacion: json["urlDocFacturacion"],
    numFolio: json["numFolio"],
    idBsale: json["idBsale"],
    tedSii: json["tedSII"],
    tipoDocumentoSii: json["tipoDocumentoSII"],
    horaTermino: json["horaTermino"],
    fechaTermino: json["fechaTermino"],
    cajaId: json["cajaID"],
    caja: json["caja"],
    tiposVentaId: json["tiposVentaID"],
    tiposVenta: TiposVenta.fromJson(json["tiposVenta"]),
    sucursalId: json["sucursalID"],
    sucursal: json["sucursal"] == null ? null : Sucursal.fromJson(json["sucursal"]),
    detalleDescuentoId: json["detalleDescuentoID"],
    detalleDescuento: json["detalleDescuento"],
    cierreCajaId: json["cierreCajaID"],
    cierreCaja: json["cierreCaja"],
    gavetaId: json["gavetaID"],
    gaveta: json["gaveta"],
    clienteId: json["clienteID"],
    cliente: json["cliente"],
    infoDeliveryId: json["infoDeliveryID"],
    infoDelivery: json["infoDelivery"],
    mediosPorOrdens: json["mediosPorOrdens"] == null ? null : List<dynamic>.from(json["mediosPorOrdens"].map((x) => x)),
    productosOrdens: List<ProductosOrden>.from(json["productosOrdens"].map((x) => ProductosOrden.fromJson(x))),
    isAnulada: json["isAnulada"],
    motivoAnulacion: json["motivoAnulacion"],
    carroProductos: json["carroProductos"] == null ? null : List<Producto>.from(json["carroProductos"].map((x) => Producto.fromJson(x))),
    carroProductosDos: json["carroProductosDos"],
    isPendiente: json["isPendiente"],
    mesaId: json["mesaID"],
    mesa: Mesa.fromJson(json["mesa"]),
    propinaPorc: json["propinaPorc"],
    numeroDeClientes: json["numeroDeClientes"],
    nombreCliente: json["nombreCliente"],
    isImpresa: json["isImpresa"],
    numOrden: json["numOrden"],
    isListo: json["isListo"],
    fechaOrdenLista: DateTime.parse(json["fechaOrdenLista"]),
    jsonFacturacionSii: json["jsonFacturacionSii"],
    jsonRespBsale: json["jsonRespBsale"],
    fechaFacturacion: DateTime.parse(json["fechaFacturacion"]),
    pagadoDelivery: json["pagadoDelivery"],
    idPedidoDelivery: json["idPedidoDelivery"],
    listaMediosPago: json["listaMediosPago"],
    minutosDesdeApertura: json["minutosDesdeApertura"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fecha": fecha.toIso8601String(),
    "hora": hora,
    "usuariosID": usuariosId,
    "usuarios": usuarios.toJson(),
    "pagado": pagado,
    "propinaCant": propinaCant,
    "propinaDif": propinaDif,
    "descEfectivo": descEfectivo,
    "dctoOrden": dctoOrden,
    "dctoArt": dctoArt,
    "dctoEfec": dctoEfec,
    "total": total,
    "subtotal": subtotal,
    "urlDocFacturacion": urlDocFacturacion,
    "numFolio": numFolio,
    "idBsale": idBsale,
    "tedSII": tedSii,
    "tipoDocumentoSII": tipoDocumentoSii,
    "horaTermino": horaTermino,
    "fechaTermino": fechaTermino,
    "cajaID": cajaId,
    "caja": caja,
    "tiposVentaID": tiposVentaId,
    "tiposVenta": tiposVenta.toJson(),
    "sucursalID": sucursalId,
    "sucursal": sucursal.toJson(),
    "detalleDescuentoID": detalleDescuentoId,
    "detalleDescuento": detalleDescuento,
    "cierreCajaID": cierreCajaId,
    "cierreCaja": cierreCaja,
    "gavetaID": gavetaId,
    "gaveta": gaveta,
    "clienteID": clienteId,
    "cliente": cliente,
    "infoDeliveryID": infoDeliveryId,
    "infoDelivery": infoDelivery,
    "mediosPorOrdens": List<dynamic>.from(mediosPorOrdens.map((x) => x)),
    "productosOrdens": List<dynamic>.from(productosOrdens.map((x) => x.toJson())),
    "isAnulada": isAnulada,
    "motivoAnulacion": motivoAnulacion,
    "carroProductos": List<dynamic>.from(carroProductos.map((x) => x.toJson())),
    "carroProductosDos": carroProductosDos,
    "isPendiente": isPendiente,
    "mesaID": mesaId,
    "mesa": mesa.toJson(),
    "propinaPorc": propinaPorc,
    "numeroDeClientes": numeroDeClientes,
    "nombreCliente": nombreCliente,
    "isImpresa": isImpresa,
    "numOrden": numOrden,
    "isListo": isListo,
    "fechaOrdenLista": fechaOrdenLista.toIso8601String(),
    "jsonFacturacionSii": jsonFacturacionSii,
    "jsonRespBsale": jsonRespBsale,
    "fechaFacturacion": fechaFacturacion.toIso8601String(),
    "pagadoDelivery": pagadoDelivery,
    "idPedidoDelivery": idPedidoDelivery,
    "listaMediosPago": listaMediosPago,
    "minutosDesdeApertura": minutosDesdeApertura,
  };
}








