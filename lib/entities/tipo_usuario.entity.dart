import 'package:leanpos/entities/config_tipo_usuario.entity.dart';

class TipoUsuario {
  TipoUsuario({
    this.id,
    this.descripcion,
    this.urlPantallaInicio,
    this.configTipoUsuario,
    this.sucursalId,
    this.sucursal,
  });

  int id;
  String descripcion;
  String urlPantallaInicio;
  List<ConfigTipoUsuario> configTipoUsuario;
  int sucursalId;
  dynamic sucursal;

  factory TipoUsuario.fromJson(Map<String, dynamic> json) => TipoUsuario(
    id: json["id"],
    descripcion: json["descripcion"],
    urlPantallaInicio: json["urlPantallaInicio"],
    configTipoUsuario: List<ConfigTipoUsuario>.from(json["configTipoUsuario"].map((x) => ConfigTipoUsuario.fromJson(x))),
    sucursalId: json["sucursalID"],
    sucursal: json["sucursal"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "descripcion": descripcion,
    "urlPantallaInicio": urlPantallaInicio,
    "configTipoUsuario": List<dynamic>.from(configTipoUsuario.map((x) => x.toJson())),
    "sucursalID": sucursalId,
    "sucursal": sucursal,
  };
}