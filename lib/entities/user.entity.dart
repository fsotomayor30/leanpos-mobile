// To parse this JSON data, do
//
//     final usuario = usuarioFromJson(jsonString);

import 'dart:convert';

import 'package:leanpos/entities/caja.entity.dart';
import 'package:leanpos/entities/sucursal.entity.dart';
import 'package:leanpos/entities/tipo_usuario.entity.dart';

Usuario usuarioFromJson(String str) => Usuario.fromJson(json.decode(str));

String usuarioToJson(Usuario data) => json.encode(data.toJson());

class Usuario {
  Usuario({
    this.id,
    this.pin,
    this.nombre,
    this.apellido,
    this.tipoUsuarioId,
    this.tipoUsuario,
    this.cajaId,
    this.caja,
    this.sucursalId,
    this.sucursal,
    this.jwt,
    this.isSuperAdmin,
    this.isHabilitado,
  });

  int id;
  int pin;
  String nombre;
  String apellido;
  int tipoUsuarioId;
  TipoUsuario tipoUsuario;
  int cajaId;
  Caja caja;
  int sucursalId;
  Sucursal sucursal;
  dynamic jwt;
  bool isSuperAdmin;
  bool isHabilitado;

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
    id: json["id"],
    pin: json["pin"],
    nombre: json["nombre"],
    apellido: json["apellido"],
    tipoUsuarioId: json["tipoUsuarioID"],
    tipoUsuario: TipoUsuario.fromJson(json["tipoUsuario"]),
    cajaId: json["cajaID"],
    caja: Caja.fromJson(json["caja"]),
    sucursalId: json["sucursalID"],
    sucursal: Sucursal.fromJson(json["sucursal"]),
    jwt: json["jwt"],
    isSuperAdmin: json["isSuperAdmin"],
    isHabilitado: json["isHabilitado"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pin": pin,
    "nombre": nombre,
    "apellido": apellido,
    "tipoUsuarioID": tipoUsuarioId,
    "tipoUsuario": tipoUsuario.toJson(),
    "cajaID": cajaId,
    "caja": caja.toJson(),
    "sucursalID": sucursalId,
    "sucursal": sucursal.toJson(),
    "jwt": jwt,
    "isSuperAdmin": isSuperAdmin,
    "isHabilitado": isHabilitado,
  };
}

