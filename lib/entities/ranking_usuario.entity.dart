class RankingUsuario {
  RankingUsuario({
    this.usuarioId,
    this.numeroOrden,
    this.nombreUsuario,
    this.cantidad,
    this.cantidadInd,
    this.valorTotal,
    this.valorProd,
    this.precioNeto,
    this.precioBruto,
    this.porcentajeCantidad,
    this.porcentajeCantidadD,
    this.porcentajePrecio,
  });

  dynamic usuarioId;
  dynamic numeroOrden;
  String nombreUsuario;
  double cantidad;
  double cantidadInd;
  double valorTotal;
  double valorProd;
  dynamic precioNeto;
  dynamic precioBruto;
  double porcentajeCantidad;
  double porcentajeCantidadD;
  double porcentajePrecio;

  factory RankingUsuario.fromJson(Map<String, dynamic> json) => RankingUsuario(
        usuarioId: json["usuarioID"],
        numeroOrden: json["numeroOrden"],
        nombreUsuario: json["nombreUsuario"],
        cantidad: json["cantidad"],
        cantidadInd: json["cantidadInd"],
        valorTotal: json["valorTotal"],
        valorProd: json["valorProd"],
        precioNeto: json["precioNeto"],
        precioBruto: json["precioBruto"],
        porcentajeCantidad: json["porcentajeCantidad"].toDouble(),
        porcentajeCantidadD: json["porcentajeCantidadD"],
        porcentajePrecio: json["porcentajePrecio"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "usuarioID": usuarioId,
        "numeroOrden": numeroOrden,
        "nombreUsuario": nombreUsuario,
        "cantidad": cantidad,
        "cantidadInd": cantidadInd,
        "valorTotal": valorTotal,
        "valorProd": valorProd,
        "precioNeto": precioNeto,
        "precioBruto": precioBruto,
        "porcentajeCantidad": porcentajeCantidad,
        "porcentajeCantidadD": porcentajeCantidadD,
        "porcentajePrecio": porcentajePrecio,
      };
}
