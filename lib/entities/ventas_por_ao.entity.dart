class VentasPorAo {
  VentasPorAo({
    this.mes,
    this.ao,
    this.mesInt,
    this.ventaPorMes,
    this.descuentosPorMes,
    this.anulacionesMes,
    this.clientesPorMes,
    this.transac,
    this.mesVenta,
    this.cantidadTransacciones,
  });

  String mes;
  int ao;
  int mesInt;
  int ventaPorMes;
  int descuentosPorMes;
  int anulacionesMes;
  int clientesPorMes;
  int transac;
  dynamic mesVenta;
  String cantidadTransacciones;

  factory VentasPorAo.fromJson(Map<String, dynamic> json) => VentasPorAo(
        mes: json["mes"],
        ao: json["año"],
        mesInt: json["mesInt"],
        ventaPorMes: json["ventaPorMes"],
        descuentosPorMes: json["descuentosPorMes"],
        anulacionesMes: json["anulacionesMes"],
        clientesPorMes: json["clientesPorMes"],
        transac: json["transac"],
        mesVenta: json["mesVenta"],
        cantidadTransacciones: json["cantidadTransacciones"],
      );

  Map<String, dynamic> toJson() => {
        "mes": mes,
        "año": ao,
        "mesInt": mesInt,
        "ventaPorMes": ventaPorMes,
        "descuentosPorMes": descuentosPorMes,
        "anulacionesMes": anulacionesMes,
        "clientesPorMes": clientesPorMes,
        "transac": transac,
        "mesVenta": mesVenta,
        "cantidadTransacciones": cantidadTransacciones,
      };
}
