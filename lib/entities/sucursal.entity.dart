import 'package:leanpos/entities/config_pos.entity.dart';

class Sucursal {
  Sucursal({
    this.id,
    this.descripcion,
    this.configPosid,
    this.configPos,
    this.bsaleId,
    this.direccion,
    this.direccionDeli,
  });

  int id;
  String descripcion;
  int configPosid;
  ConfigPos configPos;
  int bsaleId;
  dynamic direccion;
  String direccionDeli;

  factory Sucursal.fromJson(Map<String, dynamic> json) => Sucursal(
        id: json["id"],
        descripcion: json["descripcion"],
        configPosid: json["configPOSID"] == null ? null : json["configPOSID"],
        configPos: json["configPOS"] == null
            ? null
            : ConfigPos.fromJson(json["configPOS"]),
        bsaleId: json["bsaleID"] == null ? null : json["bsaleID"],
        direccion: json["direccion"] == null ? null : json["direccion"],
        direccionDeli:
            json["direccionDeli"] == null ? null : json["direccionDeli"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "configPOSID": configPosid,
        "configPOS": configPos == null ? null : configPos.toJson(),
        "bsaleID": bsaleId,
        "direccion": direccion,
        "direccionDeli": direccionDeli,
      };
}
