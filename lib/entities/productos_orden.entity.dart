import 'package:leanpos/entities/producto.entity.dart';

class ProductosOrden {
  ProductosOrden({
    this.id,
    this.fecha,
    this.hora,
    this.cantidad,
    this.total,
    this.subtotal,
    this.montoDescuento,
    this.ordenId,
    this.tbProductoId,
    this.tbProducto,
    this.isAnulada,
    this.motivoAnulacion,
    this.isListo,
    this.modificadoresProductos,
    this.mostrarNomZona,
  });

  int id;
  DateTime fecha;
  String hora;
  int cantidad;
  int total;
  int subtotal;
  int montoDescuento;
  int ordenId;
  int tbProductoId;
  Producto tbProducto;
  bool isAnulada;
  dynamic motivoAnulacion;
  bool isListo;
  List<dynamic> modificadoresProductos;
  bool mostrarNomZona;

  factory ProductosOrden.fromJson(Map<String, dynamic> json) => ProductosOrden(
    id: json["id"],
    fecha: DateTime.parse(json["fecha"]),
    hora: json["hora"],
    cantidad: json["cantidad"],
    total: json["total"],
    subtotal: json["subtotal"],
    montoDescuento: json["montoDescuento"],
    ordenId: json["ordenID"],
    tbProductoId: json["tbProductoID"],
    tbProducto: Producto.fromJson(json["tbProducto"]),
    isAnulada: json["isAnulada"],
    motivoAnulacion: json["motivoAnulacion"],
    isListo: json["isListo"],
    modificadoresProductos: List<dynamic>.from(json["modificadoresProductos"].map((x) => x)),
    mostrarNomZona: json["mostrarNomZona"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "fecha": fecha.toIso8601String(),
    "hora": hora,
    "cantidad": cantidad,
    "total": total,
    "subtotal": subtotal,
    "montoDescuento": montoDescuento,
    "ordenID": ordenId,
    "tbProductoID": tbProductoId,
    "tbProducto": tbProducto.toJson(),
    "isAnulada": isAnulada,
    "motivoAnulacion": motivoAnulacion,
    "isListo": isListo,
    "modificadoresProductos": List<dynamic>.from(modificadoresProductos.map((x) => x)),
    "mostrarNomZona": mostrarNomZona,
  };
}