import 'package:intl/intl.dart';

String moneyFormatter(int precio) {
  var f = new NumberFormat("#,###");
  return '\$' + f.format(precio).replaceAll(',', '.');
}
