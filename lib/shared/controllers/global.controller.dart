import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:leanpos/entities/categoria.entity.dart';
import 'package:leanpos/entities/mesa.entity.dart';
import 'package:leanpos/entities/orden.entity.dart';
import 'package:leanpos/entities/producto.entity.dart';
import 'package:leanpos/entities/user.entity.dart';
import 'package:leanpos/http/orden.service.dart';
import 'package:leanpos/modules/home/controllers/home.controller.dart';
import 'package:leanpos/modules/order/controllers/order.controller.dart';
import 'package:leanpos/modules/table/controllers/table.controller.dart';

class GlobalController extends GetxController {
  Categoria _categorySelected;
  Categoria get categorySelected => _categorySelected;

  List<Categoria> _categories = [];
  List<Categoria> get categories => _categories;

  List<Producto> _products = [];
  List<Producto> get products => _products;

  Usuario _usuario;
  Usuario get usuario => _usuario;

  Mesa _mesaSelected;
  Mesa get mesaSelected => _mesaSelected;

  void setProduct(List<Producto> products) {
    _products = products;
    update();
  }

  void setCategories(List<Categoria> categories) {
    _categories = categories;
    update();
  }

  void setCategorySelected(Categoria categorySelected) {
    _categorySelected = categorySelected;
    update();
  }

  Future<void> loadProduct(Categoria categoria) async {
    // GlobalController _globalController = Get.find();

    setCategorySelected(categoria);
    // EasyLoading.show(status: 'Cargando productos...', dismissOnTap: false);

    // List<Producto> product = await ProductosService.instance.obtenerProductos(
    //     _globalController.usuario.sucursalId, _categorySelected.id);
    // setProduct(product);
    EasyLoading.dismiss();
  }

  void setUsuario(Usuario usuario) {
    _usuario = usuario;
    update();
  }

  void clearMesa() {
    _mesaSelected = null;
    update();
  }

  void setMesaSelected(Mesa mesaSelected) async {
    EasyLoading.show(status: 'Buscando orden...', dismissOnTap: false);

    HomeController _homeController = Get.find();
    _mesaSelected = mesaSelected;
    update();
    Get.put(OrderController());
    OrderController _orderController = Get.find();
    TableController _tableController = Get.find();

    if (_mesaSelected == null) {
      EasyLoading.dismiss();
      _homeController.setIndexHome(1);
    } else {
      if ((_mesaSelected.isOcupada &&
              !_mesaSelected.isPagada &&
              !_mesaSelected.isImpresa) ||
          (_mesaSelected.isOcupada &&
              !_mesaSelected.isPagada &&
              _mesaSelected.isImpresa) ||
          (_mesaSelected.isOcupada &&
              _mesaSelected.isPagada &&
              _mesaSelected.isImpresa)) {
        List<Orden> ordens =
            await OrdenesService.instance.obtenerOrdenPorMesa(_mesaSelected.id);

        if (ordens.length == 0) {
          EasyLoading.dismiss();

          Get.snackbar(
            'Ya esta pagado!',
            'La orden ya esta pagada',
            snackPosition: SnackPosition.BOTTOM,
            backgroundColor: Colors.red,
            colorText: Colors.white,
          );
          setMesaSelected(null);
          await _tableController.loadTables();
        } else {
          EasyLoading.dismiss();

          _orderController.loadOrder();
          _homeController.setIndexHome(2);
        }
      } else {
        EasyLoading.dismiss();

        _orderController.loadOrder();
        _orderController.setOrden(null);
        _homeController.setIndexHome(2);
      }
    }
  }

  @override
  void onInit() async {
    super.onInit();
  }
}
