import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class InputTextWidget extends StatefulWidget {
  final Function onChanged;
  final bool obscureText;
  final String hint;
  final String initValue;
  final int maxLength;
  final TextInputType keyboardType;
  final bool onlyNumber;
  final bool autofocus;
  InputTextWidget({@required this.onChanged, this.initValue, @required this.obscureText, @required this.hint,  this.maxLength, @required this.keyboardType, this.onlyNumber,  this.autofocus});


  @override
  _InputTextWidgetState createState() => _InputTextWidgetState();
}

class _InputTextWidgetState extends State<InputTextWidget> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: widget.initValue ,
      autofocus: widget.autofocus == null ? false :  widget.autofocus,
        maxLength: widget.maxLength == null ?  200 : widget.maxLength,
        keyboardType: widget.keyboardType,
        inputFormatters: widget.onlyNumber==null ? <TextInputFormatter>[] : widget.onlyNumber ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly] : <TextInputFormatter>[],
        decoration: InputDecoration(
            counterText: '',
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: BorderSide(color: Color(0xFF59978c))
            ),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: BorderSide(color: Color(0xFF59978c))
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: BorderSide(color: Color(0xFF59978c))
            ),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25),
                borderSide: BorderSide(color: Color(0xFF59978c))
            ),
            hintText: widget.hint,
            hintStyle: TextStyle(fontSize: 16, color: Color(0xFF59978c), fontWeight: FontWeight.w600),
            filled: true,
            fillColor: Colors.transparent,
        ),
        style: TextStyle(fontSize: 15, color: Color(0xFF59978c), fontWeight: FontWeight.w600),
        obscureText: widget.obscureText,
        cursorColor: Color(0xFF59978c),
        onChanged: (String value){
          this.widget.onChanged(value);
        }
    );
  }
}
