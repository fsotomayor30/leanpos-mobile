import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:leanpos/modules/dashboard/pages/dashboard.page.dart';
import 'package:leanpos/modules/login/pages/login.page.dart';
import 'package:leanpos/shared/controllers/global.controller.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Colors.transparent,
      ),
      child: Drawer(
        child: Container(
          width: MediaQuery.of(context).size.width * .5,
          height: MediaQuery.of(context).size.height,
          child: Container(
            padding: EdgeInsets.only(
                left: 20.0, top: 50.0, right: 20.0, bottom: 40.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(50.0),
                bottomRight: Radius.circular(50.0),
              ),
            ),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Center(child: Image.asset('assets/leanpos.png', height: 50)),
              Expanded(
                child: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: Text('Mesas',
                          style: TextStyle(
                              color: Color(0xFF707070), fontSize: 15)),
                    ),
                    GestureDetector(
                      onTap: () => Get.to(DashboardPage()),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text('Reportes',
                            style: TextStyle(
                                color: Color(0xFF707070), fontSize: 15)),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        GlobalController _globalController = Get.find();
                        if (_globalController.mesaSelected != null) {
                          _globalController.setMesaSelected(null);
                        }
                        _globalController.setUsuario(null);
                        Get.offAll(LoginPage());
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        child: Text('Cerrar Sesión',
                            style: TextStyle(
                                color: Color(0xFF707070), fontSize: 15)),
                      ),
                    ),
                  ],
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }
}
