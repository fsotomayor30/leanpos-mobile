## Run Environments

```powershell
flutter run -t lib/environments/environment_develop.dart
flutter run -d chrome -t lib/environments/environment_develop.dart
flutter run -d chrome -t lib/environments/environment_prod.dart
```

## Build APK

```powershell
flutter build appbundle -t lib/environments/environment_prod.dart  --release
flutter build apk -t lib/environments/environment_prod.dart --release
flutter build apk -t lib/environments/environment_develop.dart  --release

```