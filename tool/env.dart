import 'dart:convert';
import 'dart:io';

Future<void> main() async {
  final Map<String, dynamic> config = <String, dynamic>{
    'ENDPOINT':
        Platform.environment['ENDPOINT'] ?? 'https://pwccapi.blinamic.cl/api',
  };
  print(config.toString());
  const String filename = 'lib/.env.dart';
  File(filename).writeAsString('final environment = ${json.encode(config)};');
}
